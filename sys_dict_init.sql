-- K_WJLB 文件类别
insert into sys_dict_entry VALUES('K_WJLB','文件类别','0',' ');
insert into sys_dict_item VALUES('0','K_WJLB','图片');
insert into sys_dict_item VALUES('1','K_WJLB','视频');
insert into sys_dict_item VALUES('2','K_WJLB','DOC文档');
insert into sys_dict_item VALUES('3','K_WJLB','Excel文档');

-- SYS_IS_OPEN 菜单是否默认打开
insert into sys_dict_entry VALUES('SYS_IS_OPEN','菜单是否默认打开','0',' ');
insert into sys_dict_item VALUES('0','SYS_IS_OPEN','否');
insert into sys_dict_item VALUES('1','SYS_IS_OPEN','是');

-- SYS_WINDOW_CATE 窗口类型
insert into sys_dict_entry VALUES('SYS_WINDOW_CATE','窗口类型','0',' ');
insert into sys_dict_item VALUES('0','SYS_WINDOW_CATE','工作区TAB页');
insert into sys_dict_item VALUES('1','SYS_WINDOW_CATE','弹出窗口');

-- SYS_BRANCH_LEVEL 窗口类型
insert into sys_dict_entry VALUES('SYS_BRANCH_LEVEL','窗口类型','0',' ');
insert into sys_dict_item VALUES('0','SYS_BRANCH_LEVEL','总部');
insert into sys_dict_item VALUES('1','SYS_BRANCH_LEVEL','区部');
insert into sys_dict_item VALUES('2','SYS_BRANCH_LEVEL','分部');

-- SYS_RIGHT_FLAG 系统权限类型
insert into sys_dict_entry VALUES('SYS_RIGHT_FLAG','系统权限类型','0',' ');
insert into sys_dict_item VALUES('0','SYS_RIGHT_FLAG','操作');
insert into sys_dict_item VALUES('1','SYS_RIGHT_FLAG','操作');

-- SYS_LOCK_STATUS 系统用户锁定状态
insert into sys_dict_entry VALUES('SYS_LOCK_STATUS','系统用户锁定状态','0',' ');
insert into sys_dict_item VALUES('0','SYS_LOCK_STATUS','签退');
insert into sys_dict_item VALUES('1','SYS_LOCK_STATUS','登录');
insert into sys_dict_item VALUES('2','SYS_LOCK_STATUS','非正常签退');
insert into sys_dict_item VALUES('3','SYS_LOCK_STATUS','锁定');

-- K_XTZT 系统状态
insert into sys_dict_entry VALUES('K_XTZT','系统状态','0',' ');
insert into sys_dict_item VALUES('0','K_XTZT','正常');
insert into sys_dict_item VALUES('1','K_XTZT','关闭');
insert into sys_dict_item VALUES('2','K_XTZT','维护');

-- K_PCLZT 节点执行状态
insert into sys_dict_entry VALUES('K_PCLZT','节点执行状态','0',' ');
insert into sys_dict_item VALUES('0','K_PCLZT','未激活');
insert into sys_dict_item VALUES('1','K_PCLZT','已激活');
insert into sys_dict_item VALUES('2','K_PCLZT','作业暂停');
insert into sys_dict_item VALUES('3','K_PCLZT','作业完成');
insert into sys_dict_item VALUES('4','K_PCLZT','作业失败');
insert into sys_dict_item VALUES('5','K_PCLZT','作业中断');
insert into sys_dict_item VALUES('Z','K_PCLZT','正在处理');

-- K_JYZT 作业状态
insert into sys_dict_entry VALUES('K_JYZT','作业状态','0',' ');
insert into sys_dict_item VALUES('0','K_JYZT','处理中');
insert into sys_dict_item VALUES('1','K_JYZT','处理完成');
insert into sys_dict_item VALUES('2','K_JYZT','处理失败');
insert into sys_dict_item VALUES('3','K_JYZT','未处理');