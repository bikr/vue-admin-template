package com.beiwangshan.backend.mapper;

import com.beiwangshan.backend.model.Arg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@Mapper
public interface ArgMapper extends BaseMapper<Arg> {

    /**
     * 通过sys_code查询sys_arg表数据
     * @param sysCode 系统代码
     * @return
     */
    Arg getArgBySysCode(@Param("sysCode") String sysCode);
}
