package com.beiwangshan.backend.mapper;

import com.beiwangshan.backend.model.DictItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface DictItemMapper extends BaseMapper<DictItem> {

    /**
     * 获取具体的字典条目项
     * @param entryCode
     * @return
     */
    List<DictItem> getDictItemList(@Param("entryCode") String entryCode);
}
