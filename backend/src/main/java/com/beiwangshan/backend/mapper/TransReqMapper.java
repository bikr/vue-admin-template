package com.beiwangshan.backend.mapper;

import com.beiwangshan.backend.model.TransReq;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface TransReqMapper extends BaseMapper<TransReq> {

    /**
     * 多参数交易流水表
     * @param serialNo 交易流水
     * @param transCode 交易码
     * @param branchNo 机构码
     * @param userId 用户ID
     * @param errCode 错误
     * @param beginDate 交易开始日期
     * @param endDate 交易结束日期
     * @param beginTime 交易开始时间
     * @param endTime 交易结束时间
     * @return TransReq
     */
    List<TransReq> getTransReqList(String serialNo, String transCode, String branchNo, String userId, String errCode, String beginDate, String endDate, String beginTime, String endTime);
}
