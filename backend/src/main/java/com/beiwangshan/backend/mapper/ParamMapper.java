package com.beiwangshan.backend.mapper;

import com.beiwangshan.backend.model.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface ParamMapper extends BaseMapper<Param> {

    /**
     * 根据paramCode获取系统参数
     * @param paramCode 系统参数码
     * @return Param
     */
    Param getParamByCode(String paramCode);

    /**
     * 根据paramName获取系统参数
     * @param paramName 参数名
     * @param exact 是否模糊查找，默认1-是，0-否
     * @return ParamList
     */
    List<Param> getParamListByName(String paramName,String exact);
}
