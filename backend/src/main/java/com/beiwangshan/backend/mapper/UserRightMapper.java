package com.beiwangshan.backend.mapper;

import com.beiwangshan.backend.model.UserRight;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface UserRightMapper extends BaseMapper<UserRight> {

    /**
     * 获取一个用户权限
     *
     * @param userId 用户ID
     * @return
     */
    UserRight getUserRight(String userId);
}
