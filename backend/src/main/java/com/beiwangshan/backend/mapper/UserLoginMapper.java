package com.beiwangshan.backend.mapper;

import com.beiwangshan.backend.model.UserLogin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface UserLoginMapper extends BaseMapper<UserLogin> {

    /**
     * 获取用户登录信息
     *
     * @param userId 用户Id
     * @return UserLogin
     */
    List<UserLogin> getUserLogin(String userId);
}
