package com.beiwangshan.backend.mapper;

import com.beiwangshan.backend.model.RoleRight;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface RoleRightMapper extends BaseMapper<RoleRight> {

    /**
     * 多参数获取系统权限表
     *
     * @param transCode    交易码
     * @param subTransCode 子交易码
     * @param roleCode     权限码
     * @return
     */
    List<RoleRight> getRoleRight(String transCode, String subTransCode, String roleCode);
}
