package com.beiwangshan.backend.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beiwangshan.backend.model.Arg;
import com.beiwangshan.backend.model.Kind;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@Mapper
public interface KindMapper extends BaseMapper<Kind> {

    /**
     * 获取一个分类信息
     *
     * @param kindCode 分类码
     * @return Kind
     */
    Kind getKind(String kindCode);
}
