package com.beiwangshan.backend.mapper;

import com.beiwangshan.backend.model.DictEntry;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@Mapper
public interface DictEntryMapper extends BaseMapper<DictEntry> {
    /**
     * 根据key获取字典
     * @param key
     * @return
     */
    public List<DictEntry> getByKey(@Param("key") String key);
}
