package com.beiwangshan.backend.mapper;

import com.beiwangshan.backend.model.File;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface FileMapper extends BaseMapper<File> {

    /**
     * 通过文件编号，查询文件
     * @param fileNo
     * @return
     */
    File getFileByNo(String fileNo);
}
