package com.beiwangshan.backend.mapper;

import com.beiwangshan.backend.model.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface UserMapper extends BaseMapper<User> {

    /**
     * 获取一个系统用户
     *
     * @param userId 用户ID
     * @return
     */
    User getUser(String userId);
}
