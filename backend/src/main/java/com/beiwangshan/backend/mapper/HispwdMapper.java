package com.beiwangshan.backend.mapper;

import com.beiwangshan.backend.model.Hispwd;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface HispwdMapper extends BaseMapper<Hispwd> {

    /**
     * 通过用户ID获取用户历史密码
     * @param uerId
     * @return
     */
    Hispwd getHisPwdByUerId(String uerId);
}
