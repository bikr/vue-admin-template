package com.beiwangshan.backend.mapper;

import com.beiwangshan.backend.model.Task;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface TaskMapper extends BaseMapper<Task> {

    /**
     * 获取一个task任务
     *
     * @param taskCode 任务码
     * @return Task
     */
    Task getTask(String taskCode);

    /**
     * 多参数获取task任务
     * @param taskCode 任务码
     * @param parentTaskCode 父任务码
     * @param taskName 任务名
     * @param exact 是否精确查询，1-是，0-否，仅在传了taskName时有效
     * @return
     */
    List<Task> getTaskList(String taskCode, String parentTaskCode, String taskName,String exact);
}
