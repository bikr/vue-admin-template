package com.beiwangshan.backend.mapper;

import com.beiwangshan.backend.model.Menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface MenuMapper extends BaseMapper<Menu> {

    /**
     * 通过menuCode获取一个菜单信息
     * @param menuCode
     * @return
     */
    Menu getMenuByCode(String menuCode);

    /**
     * 通过menuName获取一个或者多个菜单信息
     *
     * @param menuName 菜单名
     * @param exact    是否精确查找，默认 1-是 0-否
     * @return
     */
    List<Menu> getMenuByName(String menuName, String exact);

    /**
     * 获取菜单列表
     * @param menuCode 菜单码
     * @param transCode 交易码
     * @param subTransCode 子交易码
     * @param menuName 菜单名
     * @param parentCode 父级菜单码
     * @return
     */
    List<Menu> getMenuList(String menuCode, String transCode, String subTransCode, String menuName, String parentCode);
}
