package com.beiwangshan.backend.mapper;

import com.beiwangshan.backend.model.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface RoleMapper extends BaseMapper<Role> {

    /**
     * 根据roleCode获取权限数据
     *
     * @param roleCode 权限码
     * @return Role
     */
    Role getRoleByCode(String roleCode);
}
