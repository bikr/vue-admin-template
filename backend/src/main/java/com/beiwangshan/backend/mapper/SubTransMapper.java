package com.beiwangshan.backend.mapper;

import com.beiwangshan.backend.model.SubTrans;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface SubTransMapper extends BaseMapper<SubTrans> {

    /**
     * 多参数获取子交易
     * @param transCode 交易码
     * @param subTransCode 子交易码
     * @param subTransName 子交易名
     * @param exact 是否精确查询，1-是，0-否，仅在传了subTranName时有效
     * @return SubTrans
     */
    List<SubTrans> getSubTransList(String transCode, String subTransCode, String subTransName,String exact);
}
