package com.beiwangshan.backend.mapper;

import com.beiwangshan.backend.model.RoleUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface RoleUserMapper extends BaseMapper<RoleUser> {

    /**
     * 多参数获取用户权限
     *
     * @param userCode 用户码
     * @param roleCode 权限码
     * @return RoleUser
     */
    List<RoleUser> getRoleUser(String userCode, String roleCode);
}
