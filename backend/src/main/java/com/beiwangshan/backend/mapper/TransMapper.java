package com.beiwangshan.backend.mapper;

import com.beiwangshan.backend.model.Trans;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface TransMapper extends BaseMapper<Trans> {

    /**
     * 获取交易码信息
     *
     * @param transCode 交易码
     * @return Trans
     */
    Trans getTrans(String transCode);

    /**
     * 多参数获取交易码信息列表
     *
     * @param transCode 交易码
     * @param transName 交易名
     * @param kindCode  分类码
     * @param exact     是否精确查询，1-是，0-否，仅在传了transName时有效
     * @return
     */
    List<Trans> getTransList(String transCode, String transName, String kindCode, String exact);
}
