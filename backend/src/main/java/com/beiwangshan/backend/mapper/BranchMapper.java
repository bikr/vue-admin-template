package com.beiwangshan.backend.mapper;

import com.beiwangshan.backend.model.Branch;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface BranchMapper extends BaseMapper<Branch> {

    /**
     * 通过branch_code获取系统的一个机构信息
     * @param brachCode
     * @return
     */
    Branch getBranchByCode(String brachCode);
}
