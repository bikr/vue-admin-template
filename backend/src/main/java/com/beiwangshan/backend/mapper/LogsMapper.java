package com.beiwangshan.backend.mapper;

import com.beiwangshan.backend.model.Logs;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface LogsMapper extends BaseMapper<Logs> {

    /**
     * 通过logSerial获取一条日志信息
     *
     * @param logSerial
     * @return
     */
    Logs getLogsByLogSerial(String logSerial);

    /**
     * 通过用户ID获取日志表中的所有用户日志
     * @param userId 用户id
     * @param transCode 交易码
     * @param subTransCode 子交易码
     * @param branchNo 机构码
     * @param beginDate 操作日期
     * @param endDate 结束日期
     * @param beginTime 操作时间
     * @param endTime 结束时间
     * @return Logs
     */
    List<Logs> getLogsByUser(String userId, String transCode, String subTransCode, String branchNo, String beginDate, String endDate, String beginTime, String endTime);
}
