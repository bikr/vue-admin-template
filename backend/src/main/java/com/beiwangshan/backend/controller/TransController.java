package com.beiwangshan.backend.controller;


import com.beiwangshan.backend.model.Trans;
import com.beiwangshan.backend.service.TransService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@RestController
@RequestMapping("/func")
public class TransController {

    @Autowired
    private TransService transService;

    /**
     * 获取交易码信息
     * @param transCode 交易码
     * @return Trans
     */
    @GetMapping("/100020")
    public Trans getTrans(@RequestParam(value = "transCode")String transCode){
        return transService.getTrans(transCode);
    }

    /**
     * 多参数获取交易码信息列表
     * @param transCode 交易码
     * @param transName 交易名
     * @param kindCode 分类码
     * @param exact 是否精确查询，1-是，0-否，仅在传了transName时有效
     * @return
     */
    @GetMapping("/100021")
    public List<Trans> getTransList(@RequestParam(value = "transCode",required = false)String transCode,
                                    @RequestParam(value = "transName",required = false)String transName,
                                    @RequestParam(value = "kindCode",required = false)String kindCode,
                                    @RequestParam(value = "exact",required = false,defaultValue = "1")String exact){
        return transService.getTransList(transCode,transName,kindCode,exact);
    }
}
