package com.beiwangshan.backend.controller;


import com.beiwangshan.backend.model.Branch;
import com.beiwangshan.backend.service.BranchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@RestController
@RequestMapping("/func")
public class BranchController {

    @Autowired
    private BranchService branchService;

    /**
     * 通过branch_code获取系统的一个机构信息
     * @param branchCode
     * @return
     */
    @GetMapping("/100002")
    public Branch getBranchByCode(@RequestParam(value = "branchCode")String branchCode){
        return branchService.getBranchByCode(branchCode);
    }
}
