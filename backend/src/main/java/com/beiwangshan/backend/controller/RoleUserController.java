package com.beiwangshan.backend.controller;


import com.beiwangshan.backend.model.RoleUser;
import com.beiwangshan.backend.service.RoleUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@RestController
@RequestMapping("/func")
public class RoleUserController {
    @Autowired
    private RoleUserService roleUserService;

    /**
     * 多参数获取用户权限
     * @param userCode 用户码
     * @param roleCode 权限码
     * @return RoleUser
     */
    @GetMapping("/100016")
    public List<RoleUser> getRoleUser(@RequestParam(value = "userCode",required = false)String userCode,
                                      @RequestParam(value = "roleCode",required = false)String roleCode){
        return roleUserService.getRoleUser(userCode,roleCode);
    }
}
