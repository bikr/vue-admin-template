package com.beiwangshan.backend.controller;


import com.beiwangshan.backend.model.Logs;
import com.beiwangshan.backend.service.LogsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@RestController
@RequestMapping("/func")
public class LogsController {
    @Autowired
    private LogsService logsService;

    /**
     * 通过logSerial获取一条日志信息
     * @param logSerial 日志号
     * @return
     */
    @GetMapping("/100007")
    public Logs getLogsByLogSerial(@RequestParam("logSerial")String logSerial){
        return logsService.getLogsByLogSerial(logSerial);
    }

    /**
     * 通过用户ID获取日志表中的所有用户日志
     * @param userId 用户id
     * @param transCode 交易码
     * @param subTransCode 子交易码
     * @param branchNo 机构码
     * @param beginDate 操作日期
     * @param endDate 结束日期
     * @param beginTime 操作时间
     * @param endTime 结束时间
     * @return Logs
     */
    @GetMapping("/100008")
    public List<Logs> getLogsByUser(@RequestParam(value = "userId",required = false)String userId,
                                    @RequestParam(value = "transCode",required = false)String transCode,
                                    @RequestParam(value = "subTransCode",required = false)String subTransCode,
                                    @RequestParam(value = "branchNo",required = false)String branchNo,
                                    @RequestParam(value = "beginDate",required = false)String beginDate,
                                    @RequestParam(value = "endDate",required = false)String endDate,
                                    @RequestParam(value = "opTime",required = false)String beginTime,
                                    @RequestParam(value = "endTime",required = false)String endTime){
        return logsService.getLogsByUser(userId,transCode,subTransCode,branchNo,beginDate,endDate,beginTime,endTime);
    }
}
