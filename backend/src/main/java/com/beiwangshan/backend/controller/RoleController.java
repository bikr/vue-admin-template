package com.beiwangshan.backend.controller;


import com.beiwangshan.backend.model.Role;
import com.beiwangshan.backend.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@RestController
@RequestMapping("/func")
public class RoleController {
    @Autowired
    private RoleService roleService;

    /**
     * 根据roleCode获取权限数据
     * @param roleCode 权限码
     * @return Role
     */
    @GetMapping("/100014")
    public Role getRoleByCode(@RequestParam(value = "roleCode")String roleCode){
        return roleService.getRoleByCode(roleCode);
    }
}
