package com.beiwangshan.backend.controller;


import com.beiwangshan.backend.model.SubTrans;
import com.beiwangshan.backend.service.SubTransService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@RestController
@RequestMapping("/func")
public class SubTransController {
    @Autowired
    private SubTransService subTransService;

    /**
     * 多参数获取子交易
     * @param transCode 交易码
     * @param subTransCode 子交易码
     * @param subTransName 子交易名
     * @param exact 是否精确查询，1-是，0-否，仅在传了subTranName时有效
     * @return SubTrans
     */
    @GetMapping("/100017")
    public List<SubTrans> getSubTransList(@RequestParam(value = "transCode",required = false)String transCode,
                                          @RequestParam(value = "subTransCode",required = false)String subTransCode,
                                          @RequestParam(value = "subTransName",required = false)String subTransName,
                                          @RequestParam(value = "exact",required = false,defaultValue = "0")String exact){
        return subTransService.getSubTransList(transCode,subTransCode,subTransName,exact);
    }
}
