package com.beiwangshan.backend.controller;


import com.beiwangshan.backend.model.Param;
import com.beiwangshan.backend.service.ParamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@RestController
@RequestMapping("/func")
public class ParamController {

    @Autowired
    private ParamService paramService;

    /**
     * 根据paramCode获取系统参数
     * @param paramCode 系统参数码
     * @return Param
     */
    @GetMapping("/100012")
    public Param getParamByCode(@RequestParam(value = "paramCode")String paramCode){
        return paramService.getParamByCode(paramCode);
    }

    /**
     * 根据paramName获取系统参数
     * @param paramName 参数名
     * @param exact 是否模糊查找，默认1-是，0-否
     * @return ParamList
     */
    @GetMapping("/100013")
    public List<Param> getParamListByName(@RequestParam(value = "paramName")String paramName,
                                          @RequestParam(value = "exact",defaultValue = "1",required = false)String exact){
        return paramService.getParamListByName(paramName,exact);
    }
}
