package com.beiwangshan.backend.controller;


import com.beiwangshan.backend.model.UserRight;
import com.beiwangshan.backend.service.UserRightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@RestController
@RequestMapping("/func")
public class UserRightController {

    @Autowired
    private UserRightService userRightService;

    /**
     * 获取一个用户权限
     * @param userId 用户ID
     * @return
     */
    @GetMapping("/100026")
    public UserRight getUserRight(@RequestParam(value = "userId")String userId){
        return userRightService.getUserRight(userId);
    }
}
