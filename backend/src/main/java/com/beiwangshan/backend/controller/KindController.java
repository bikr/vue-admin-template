package com.beiwangshan.backend.controller;


import com.beiwangshan.backend.model.Kind;
import com.beiwangshan.backend.service.KindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@RestController
@RequestMapping("/func")
public class KindController {

    @Autowired
    private KindService kindService;

    /**
     * 获取一个分类信息
     * @param kindCode 分类码
     * @return Kind
     */
    @GetMapping("/100023")
    public Kind getKind(@RequestParam(value = "kindCode")String kindCode){
        return kindService.getKind(kindCode);
    }
}
