package com.beiwangshan.backend.controller;


import com.beiwangshan.backend.model.Hispwd;
import com.beiwangshan.backend.service.HispwdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@RestController
@RequestMapping("/func")
public class HispwdController {

    @Autowired
    private HispwdService hispwdService;

    /**
     * 通过用户ID获取用户历史密码
     * @param uerId
     * @return
     */
    @GetMapping("/100006")
    public Hispwd getHisPwdByUerId(@RequestParam("userId")String uerId){
        return hispwdService.getHisPwdByUerId(uerId);
    }
}
