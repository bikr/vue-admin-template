package com.beiwangshan.backend.controller;


import com.beiwangshan.backend.model.TransReq;
import com.beiwangshan.backend.service.TransReqService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@RestController
@RequestMapping("/func")
public class TransReqController {
    @Autowired
    private TransReqService transReqService;

    /**
     * 多参数获取交易流水表
     * @param serialNo 交易流水码
     * @param transCode 交易码
     * @param branchNo 机构码
     * @param userId 用户ID
     * @param errCode 错误码
     * @param beginDate 交易开始日期
     * @param endDate 交易结束日期
     * @param beginTime 交易开始时间
     * @param endTime 交易结束时间
     * @return TransReq
     */
    @GetMapping("/100022")
    public List<TransReq> getTransReqList(@RequestParam(value = "serialNo",required = false)String serialNo,
                                          @RequestParam(value = "transCode",required = false)String transCode,
                                          @RequestParam(value = "branchNo",required = false)String branchNo,
                                          @RequestParam(value = "userId",required = false)String userId,
                                          @RequestParam(value = "errCode",required = false)String errCode,
                                          @RequestParam(value = "beginDate",required = false)String beginDate,
                                          @RequestParam(value = "endDate",required = false)String endDate,
                                          @RequestParam(value = "beginTime",required = false)String beginTime,
                                          @RequestParam(value = "endTime",required = false)String endTime){
        return transReqService.getTransReqList(serialNo,transCode,branchNo,userId,errCode,beginDate,endDate,beginTime,endTime);
    }
}
