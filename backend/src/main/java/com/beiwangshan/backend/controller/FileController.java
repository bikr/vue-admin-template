package com.beiwangshan.backend.controller;


import com.beiwangshan.backend.model.File;
import com.beiwangshan.backend.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@RestController
@RequestMapping("/func")
public class FileController {
    @Autowired
    private FileService fileService;

    /**
     * 通过文件编号，查询文件
     * @param fileNo
     * @return
     */
    @GetMapping("/100005")
    public File getFileByNo(@RequestParam("fileNo")String fileNo){
        return fileService.getFileByNo(fileNo);
    }
}
