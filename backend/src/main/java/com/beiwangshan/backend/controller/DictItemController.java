package com.beiwangshan.backend.controller;


import com.beiwangshan.backend.model.DictItem;
import com.beiwangshan.backend.service.DictItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@RestController
@RequestMapping("/func")
public class DictItemController {

    @Autowired
    private DictItemService dictItemService;

    @GetMapping("/100004")
    public List<DictItem> getDictItemList(@RequestParam(value = "entryCode") String entryCode){
        return dictItemService.getDictItemList(entryCode);
    }

    @GetMapping("/test")
    public Map<String, Object> test() {
        HashMap<String, Object> data = new HashMap<>();
        data.put("info", "测试成功");
        return data;
    }
}
