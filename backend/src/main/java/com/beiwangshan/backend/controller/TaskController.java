package com.beiwangshan.backend.controller;


import com.beiwangshan.backend.model.Task;
import com.beiwangshan.backend.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@RestController
@RequestMapping("/func")
public class TaskController {
    @Autowired
    private TaskService taskService;

    /**
     * 获取一个task任务
     * @param taskCode 任务码
     * @return Task
     */
    @GetMapping("/100018")
    public Task getTask(@RequestParam(value = "taskCode")String taskCode){
        return taskService.getTask(taskCode);
    }

    /**
     * 多参数获取task任务
     * @param taskCode 任务码
     * @param parentTaskCode 父任务码
     * @param taskName 任务名
     * @param exact 是否精确查询，1-是，0-否，仅在传了taskName时有效
     * @return
     */
    @GetMapping("/100019")
    public List<Task> getTaskList(@RequestParam(value = "taskCode",required = false)String taskCode,
                                  @RequestParam(value = "parentTaskCode",required = false)String parentTaskCode,
                                  @RequestParam(value = "taskName",required = false)String taskName,
                                  @RequestParam(value = "exact",required = false,defaultValue = "1")String exact){
        return taskService.getTaskList(taskCode,parentTaskCode,taskName,exact);
    }
}
