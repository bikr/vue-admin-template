package com.beiwangshan.backend.controller;


import com.beiwangshan.backend.model.RoleRight;
import com.beiwangshan.backend.service.RoleRightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@RestController
@RequestMapping("/func")
public class RoleRightController {
    @Autowired
    private RoleRightService roleRightService;

    /**
     * 多参数获取系统权限表
     * @param transCode 交易码
     * @param subTransCode 子交易码
     * @param roleCode 权限码
     * @return
     */
    @GetMapping("/100015")
    public List<RoleRight> getRoleRight(@RequestParam(value = "transCode",required = false)String transCode,
                                        @RequestParam(value = "subTransCode",required = false)String subTransCode,
                                        @RequestParam(value = "roleCode",required = false)String roleCode){
        return roleRightService.getRoleRight(transCode,subTransCode,roleCode);
    }
}
