package com.beiwangshan.backend.controller;


import com.beiwangshan.backend.model.Arg;
import com.beiwangshan.backend.service.ArgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@RestController
@RequestMapping("/func")
public class ArgController {

    @Autowired
    private ArgService argService;

    /**
     * 100003 通过sysCode获取系统参数
     * @param sysCode
     * @return
     */
    @GetMapping("/100001")
    public Arg getArgBySysCode(@RequestParam(value = "sysCode") String sysCode){
        return argService.getArgBySysCode(sysCode);
    }
}
