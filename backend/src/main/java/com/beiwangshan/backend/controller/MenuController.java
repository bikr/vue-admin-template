package com.beiwangshan.backend.controller;


import com.beiwangshan.backend.model.Menu;
import com.beiwangshan.backend.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@RestController
@RequestMapping("/func")
public class MenuController {

    @Autowired
    private MenuService menuService;

    /**
     * 通过menuCode获取一个菜单信息
     * @param menuCode 菜单码
     * @return
     */
    @GetMapping("/100009")
    public Menu getMenuByCode(@RequestParam(value = "menuCode")String menuCode){
        return menuService.getMenuByCode(menuCode);
    }

    /**
     * 通过menuName获取一个或者多个菜单信息
     * @param menuName 菜单名
     * @param exact 是否精确查找，默认 1-是 0-否
     * @return
     */
    @GetMapping("/100010")
    public List<Menu> getMenuByName(@RequestParam(value = "menuName")String menuName,
                              @RequestParam(value = "exact",defaultValue = "1",required = false)String exact){
        return menuService.getMenuByName(menuName,exact);
    }

    /**
     * 获取菜单列表
     * @param menuCode 菜单码
     * @param transCode 交易码
     * @param subTransCode 子交易码
     * @param menuName 菜单名
     * @param parentCode 父级菜单码
     * @return
     */
    @GetMapping("/100011")
    public List<Menu> getMenuList(@RequestParam(value = "menuCode",required = false)String menuCode,
                                  @RequestParam(value = "transCode",required = false)String transCode,
                                  @RequestParam(value = "subTransCode",required = false)String subTransCode,
                                  @RequestParam(value = "menuName",required = false)String menuName,
                                  @RequestParam(value = "parentCode",required = false)String parentCode){
       return menuService.getMenuList(menuCode,transCode,subTransCode,menuName,parentCode);
    }
}
