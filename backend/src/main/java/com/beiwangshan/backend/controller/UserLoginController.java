package com.beiwangshan.backend.controller;


import com.beiwangshan.backend.model.UserLogin;
import com.beiwangshan.backend.service.UserLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@RestController
@RequestMapping("/func")
public class UserLoginController {
    @Autowired
    private UserLoginService userLoginService;

    /**
     * 获取用户登录信息
     * @param userId 用户Id
     * @return UserLogin
     */
    @GetMapping("/100025")
    public List<UserLogin>  getUserLogin(@RequestParam(value = "userId")String userId){
        return userLoginService.getUserLogin(userId);
    }
}
