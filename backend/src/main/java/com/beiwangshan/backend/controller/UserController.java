package com.beiwangshan.backend.controller;


import com.beiwangshan.backend.model.User;
import com.beiwangshan.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@RestController
@RequestMapping("/func")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 获取一个系统用户
     * @param userId 用户ID
     * @return
     */
    @GetMapping("/100024")
    public User getUser(@RequestParam(value = "userId")String userId){
        return userService.getUser(userId);
    }
}
