package com.beiwangshan.backend.controller;


import com.beiwangshan.backend.model.DictEntry;
import com.beiwangshan.backend.service.DictEntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@RestController
@RequestMapping("/func")
public class DictEntryController {

    @Autowired
    private DictEntryService dictEntryService;

    @GetMapping("/100003")
    public List<DictEntry> getDictEntry(@RequestParam(value = "entryCode",required = true)String entryCode){
        return dictEntryService.getByKey(entryCode);
    }

}
