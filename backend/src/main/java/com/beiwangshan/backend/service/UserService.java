package com.beiwangshan.backend.service;

import com.beiwangshan.backend.model.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface UserService extends IService<User> {

    /**
     * 获取一个系统用户
     * @param userId 用户ID
     * @return
     */
    User getUser(String userId);
}
