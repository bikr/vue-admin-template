package com.beiwangshan.backend.service.impl;

import com.beiwangshan.backend.model.Branch;
import com.beiwangshan.backend.mapper.BranchMapper;
import com.beiwangshan.backend.service.BranchService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@Service
public class BranchServiceImpl extends ServiceImpl<BranchMapper, Branch> implements BranchService {

    @Autowired
    private BranchMapper branchMapper;
    /**
     * 通过branch_code获取系统的一个机构信息
     *
     * @param brachCode
     * @return
     */
    @Override
    public Branch getBranchByCode(String brachCode) {
        return branchMapper.getBranchByCode(brachCode);
    }
}
