package com.beiwangshan.backend.service;

import com.beiwangshan.backend.model.Role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface RoleService extends IService<Role> {

    /**
     * 根据roleCode获取权限数据
     * @param roleCode 权限码
     * @return Role
     */
    Role getRoleByCode(String roleCode);
}
