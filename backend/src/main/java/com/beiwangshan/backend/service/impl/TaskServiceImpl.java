package com.beiwangshan.backend.service.impl;

import com.beiwangshan.backend.model.Task;
import com.beiwangshan.backend.mapper.TaskMapper;
import com.beiwangshan.backend.service.TaskService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@Service
public class TaskServiceImpl extends ServiceImpl<TaskMapper, Task> implements TaskService {

    @Autowired
    private TaskMapper taskMapper;
    /**
     * 获取一个task任务
     *
     * @param taskCode 任务码
     * @return Task
     */
    @Override
    public Task getTask(String taskCode) {
        return taskMapper.getTask(taskCode);
    }

    /**
     * 多参数获取task任务
     * @param taskCode 任务码
     * @param parentTaskCode 父任务码
     * @param taskName 任务名
     * @param exact 是否精确查询，1-是，0-否，仅在传了taskName时有效
     * @return
     */
    @Override
    public List<Task> getTaskList(String taskCode, String parentTaskCode, String taskName,String exact) {
        return taskMapper.getTaskList(taskCode,parentTaskCode,taskName,exact);
    }
}
