package com.beiwangshan.backend.service;

import com.beiwangshan.backend.model.DictEntry;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface DictEntryService extends IService<DictEntry> {
    /**
     * 通过key获取参数
     * @param key
     * @return
     */
    List<DictEntry> getByKey(String key);
}
