package com.beiwangshan.backend.service.impl;

import com.beiwangshan.backend.model.RoleRight;
import com.beiwangshan.backend.mapper.RoleRightMapper;
import com.beiwangshan.backend.service.RoleRightService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@Service
public class RoleRightServiceImpl extends ServiceImpl<RoleRightMapper, RoleRight> implements RoleRightService {

    @Autowired
    private RoleRightMapper roleRightMapper;
    /**
     * 多参数获取系统权限表
     *
     * @param transCode    交易码
     * @param subTransCode 子交易码
     * @param roleCode     权限码
     * @return
     */
    @Override
    public List<RoleRight> getRoleRight(String transCode, String subTransCode, String roleCode) {
        return roleRightMapper.getRoleRight(transCode,subTransCode,roleCode);
    }
}
