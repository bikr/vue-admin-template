package com.beiwangshan.backend.service.impl;

import com.beiwangshan.backend.model.Logs;
import com.beiwangshan.backend.mapper.LogsMapper;
import com.beiwangshan.backend.service.LogsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@Service
public class LogsServiceImpl extends ServiceImpl<LogsMapper, Logs> implements LogsService {

    @Autowired
    private LogsMapper logsMapper;

    /**
     * 通过logSerial获取一条日志信息
     *
     * @param logSerial
     * @return
     */
    @Override
    public Logs getLogsByLogSerial(String logSerial) {
        return logsMapper.getLogsByLogSerial(logSerial);
    }

    /**
     * 通过用户ID获取日志表中的所有用户日志
     * @param userId 用户id
     * @param transCode 交易码
     * @param subTransCode 子交易码
     * @param branchNo 机构码
     * @param beginDate 操作日期
     * @param endDate 结束日期
     * @param beginTime 操作时间
     * @param endTime 结束时间
     * @return Logs
     */
    @Override
    public List<Logs> getLogsByUser(String userId, String transCode, String subTransCode, String branchNo, String beginDate, String endDate, String beginTime, String endTime) {
        return logsMapper.getLogsByUser(userId,transCode,subTransCode,branchNo,beginDate,endDate,beginTime,endTime);
    }
}
