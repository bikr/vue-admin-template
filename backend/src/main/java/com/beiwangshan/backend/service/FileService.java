package com.beiwangshan.backend.service;

import com.beiwangshan.backend.model.File;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface FileService extends IService<File> {

    /**
     * 通过文件编号，查询文件
     * @param fileNo
     * @return
     */
    File getFileByNo(String fileNo);
}
