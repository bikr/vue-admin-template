package com.beiwangshan.backend.service.impl;

import com.beiwangshan.backend.model.Trans;
import com.beiwangshan.backend.mapper.TransMapper;
import com.beiwangshan.backend.service.TransService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@Service
public class TransServiceImpl extends ServiceImpl<TransMapper, Trans> implements TransService {

    @Autowired
    private TransMapper transMapper;
    /**
     * 获取交易码信息
     *
     * @param transCode 交易码
     * @return Trans
     */
    @Override
    public Trans getTrans(String transCode) {
        return transMapper.getTrans(transCode);
    }

    /**
     * 多参数获取交易码信息列表
     *
     * @param transCode 交易码
     * @param transName 交易名
     * @param kindCode  分类码
     * @param exact     是否精确查询，1-是，0-否，仅在传了transName时有效
     * @return
     */
    @Override
    public List<Trans> getTransList(String transCode, String transName, String kindCode, String exact) {
        return transMapper.getTransList(transCode,transName,kindCode,exact);
    }
}
