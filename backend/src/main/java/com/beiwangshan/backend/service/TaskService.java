package com.beiwangshan.backend.service;

import com.beiwangshan.backend.model.Task;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface TaskService extends IService<Task> {

    /**
     * 获取一个task任务
     * @param taskCode 任务码
     * @return Task
     */
    Task getTask(String taskCode);

    /**
     * 多参数获取task任务
     * @param taskCode 任务码
     * @param parentTaskCode 父任务码
     * @param taskName 任务名
     * @param exact 是否精确查询，1-是，0-否，仅在传了taskName时有效
     * @return
     */
    List<Task> getTaskList(String taskCode, String parentTaskCode, String taskName,String exact);
}
