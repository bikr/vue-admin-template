package com.beiwangshan.backend.service.impl;

import com.beiwangshan.backend.model.UserRight;
import com.beiwangshan.backend.mapper.UserRightMapper;
import com.beiwangshan.backend.service.UserRightService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@Service
public class UserRightServiceImpl extends ServiceImpl<UserRightMapper, UserRight> implements UserRightService {

    @Autowired
    private UserRightMapper userRightMapper;
    /**
     * 获取一个用户权限
     *
     * @param userId 用户ID
     * @return
     */
    @Override
    public UserRight getUserRight(String userId) {
        return userRightMapper.getUserRight(userId);
    }
}
