package com.beiwangshan.backend.service;

import com.beiwangshan.backend.model.DictItem;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface DictItemService extends IService<DictItem> {

    List<DictItem> getDictItemList(String entryCode);
}
