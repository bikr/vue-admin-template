package com.beiwangshan.backend.service;

import com.beiwangshan.backend.model.Arg;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface ArgService extends IService<Arg> {

    /**
     * 100003 通过sysCode获取系统参数
     * @param sysCode
     * @return
     */
    Arg getArgBySysCode(String sysCode);
}
