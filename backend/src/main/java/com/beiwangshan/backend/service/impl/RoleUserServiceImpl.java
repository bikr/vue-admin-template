package com.beiwangshan.backend.service.impl;

import com.beiwangshan.backend.model.RoleUser;
import com.beiwangshan.backend.mapper.RoleUserMapper;
import com.beiwangshan.backend.service.RoleUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@Service
public class RoleUserServiceImpl extends ServiceImpl<RoleUserMapper, RoleUser> implements RoleUserService {

    @Autowired
    private RoleUserMapper roleUserMapper;
    /**
     * 多参数获取用户权限
     *
     * @param userCode 用户码
     * @param roleCode 权限码
     * @return RoleUser
     */
    @Override
    public List<RoleUser> getRoleUser(String userCode, String roleCode) {
        return roleUserMapper.getRoleUser(userCode,roleCode);
    }
}
