package com.beiwangshan.backend.service.impl;

import com.beiwangshan.backend.model.User;
import com.beiwangshan.backend.mapper.UserMapper;
import com.beiwangshan.backend.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private UserMapper userMapper;
    /**
     * 获取一个系统用户
     *
     * @param userId 用户ID
     * @return
     */
    @Override
    public User getUser(String userId) {
        return userMapper.getUser(userId);
    }
}
