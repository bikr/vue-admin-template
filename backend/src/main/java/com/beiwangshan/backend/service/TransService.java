package com.beiwangshan.backend.service;

import com.beiwangshan.backend.model.Trans;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface TransService extends IService<Trans> {

    /**
     * 获取交易码信息
     * @param transCode 交易码
     * @return Trans
     */
    Trans getTrans(String transCode);

    /**
     * 多参数获取交易码信息列表
     * @param transCode 交易码
     * @param transName 交易名
     * @param kindCode 分类码
     * @param exact 是否精确查询，1-是，0-否，仅在传了transName时有效
     * @return
     */
    List<Trans> getTransList(String transCode, String transName, String kindCode, String exact);
}
