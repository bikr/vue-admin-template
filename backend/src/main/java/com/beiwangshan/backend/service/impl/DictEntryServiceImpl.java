package com.beiwangshan.backend.service.impl;

import com.beiwangshan.backend.model.DictEntry;
import com.beiwangshan.backend.mapper.DictEntryMapper;
import com.beiwangshan.backend.service.DictEntryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@Service
public class DictEntryServiceImpl extends ServiceImpl<DictEntryMapper, DictEntry> implements DictEntryService {

    @Autowired
    private DictEntryMapper dictEntryMapper;

    @Override
    public List<DictEntry> getByKey(String key) {
        return dictEntryMapper.getByKey(key);
    }
}
