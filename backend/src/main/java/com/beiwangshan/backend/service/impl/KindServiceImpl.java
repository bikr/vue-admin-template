package com.beiwangshan.backend.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beiwangshan.backend.mapper.KindMapper;
import com.beiwangshan.backend.model.Kind;
import com.beiwangshan.backend.service.KindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@Service
public class KindServiceImpl extends ServiceImpl<KindMapper, Kind> implements KindService {

    @Autowired
    private KindMapper kindMapper;

    /**
     * 获取一个分类信息
     *
     * @param kindCode 分类码
     * @return Kind
     */
    @Override
    public Kind getKind(String kindCode) {
        return kindMapper.getKind(kindCode);
    }
}
