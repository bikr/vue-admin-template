package com.beiwangshan.backend.service.impl;

import com.beiwangshan.backend.model.Menu;
import com.beiwangshan.backend.mapper.MenuMapper;
import com.beiwangshan.backend.service.MenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements MenuService {

    @Autowired
    private MenuMapper menuMapper;
    /**
     * 通过menuCode获取一个菜单信息
     *
     * @param menuCode
     * @return
     */
    @Override
    public Menu getMenuByCode(String menuCode) {
        return menuMapper.getMenuByCode(menuCode);
    }

    /**
     * 通过menuName获取一个或者多个菜单信息
     *
     * @param menuName 菜单名
     * @param exact    是否精确查找，默认 1-是 0-否
     * @return
     */
    @Override
    public List<Menu> getMenuByName(String menuName, String exact) {
        return menuMapper.getMenuByName(menuName,exact);
    }

    /**
     * 获取菜单列表
     *
     * @param menuCode     菜单码
     * @param transCode    交易码
     * @param subTransCode 子交易码
     * @param menuName     菜单名
     * @param parentCode   父级菜单码
     * @return
     */
    @Override
    public List<Menu> getMenuList(String menuCode, String transCode, String subTransCode, String menuName, String parentCode) {
        return menuMapper.getMenuList(menuCode,transCode,subTransCode,menuName,parentCode);
    }
}
