package com.beiwangshan.backend.service.impl;

import com.beiwangshan.backend.model.Param;
import com.beiwangshan.backend.mapper.ParamMapper;
import com.beiwangshan.backend.service.ParamService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@Service
public class ParamServiceImpl extends ServiceImpl<ParamMapper, Param> implements ParamService {

    @Autowired
    private ParamMapper paramMapper;

    /**
     * 根据paramCode获取系统参数
     * @param paramCode 系统参数码
     * @return Param
     */
    @Override
    public Param getParamByCode(String paramCode) {
        return paramMapper.getParamByCode(paramCode);
    }

    /**
     * 根据paramName获取系统参数
     * @param paramName 参数名
     * @param exact 是否模糊查找，默认1-是，0-否
     * @return ParamList
     */
    @Override
    public List<Param> getParamListByName(String paramName,String exact) {
        return paramMapper.getParamListByName(paramName,exact);
    }
}
