package com.beiwangshan.backend.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beiwangshan.backend.model.Kind;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface KindService extends IService<Kind> {

    /**
     * 获取一个分类信息
     * @param kindCode 分类码
     * @return Kind
     */
    Kind getKind(String kindCode);
}
