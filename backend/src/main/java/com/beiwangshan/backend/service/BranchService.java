package com.beiwangshan.backend.service;

import com.beiwangshan.backend.model.Branch;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface BranchService extends IService<Branch> {

    /**
     * 通过branch_code获取系统的一个机构信息
     * @param brachCode
     * @return
     */
    Branch getBranchByCode(String brachCode);
}
