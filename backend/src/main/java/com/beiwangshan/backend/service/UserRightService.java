package com.beiwangshan.backend.service;

import com.beiwangshan.backend.model.UserRight;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface UserRightService extends IService<UserRight> {

    /**
     * 获取一个用户权限
     * @param userId 用户ID
     * @return
     */
    UserRight getUserRight(String userId);
}
