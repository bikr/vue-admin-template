package com.beiwangshan.backend.service.impl;

import com.beiwangshan.backend.model.SubTrans;
import com.beiwangshan.backend.mapper.SubTransMapper;
import com.beiwangshan.backend.service.SubTransService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@Service
public class SubTransServiceImpl extends ServiceImpl<SubTransMapper, SubTrans> implements SubTransService {

    @Autowired
    private SubTransMapper subTransMapper;
    /**
     * 多参数获取子交易
     * @param transCode 交易码
     * @param subTransCode 子交易码
     * @param subTransName 子交易名
     * @param exact 是否精确查询，1-是，0-否，仅在传了subTranName时有效
     * @return SubTrans
     */
    @Override
    public List<SubTrans> getSubTransList(String transCode, String subTransCode, String subTransName,String exact) {
        return subTransMapper.getSubTransList(transCode,subTransCode,subTransName,exact);
    }
}
