package com.beiwangshan.backend.service;

import com.beiwangshan.backend.model.RoleUser;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface RoleUserService extends IService<RoleUser> {

    /**
     * 多参数获取用户权限
     * @param userCode 用户码
     * @param roleCode 权限码
     * @return RoleUser
     */
    List<RoleUser> getRoleUser(String userCode, String roleCode);
}
