package com.beiwangshan.backend.service.impl;

import com.beiwangshan.backend.model.UserLogin;
import com.beiwangshan.backend.mapper.UserLoginMapper;
import com.beiwangshan.backend.service.UserLoginService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@Service
public class UserLoginServiceImpl extends ServiceImpl<UserLoginMapper, UserLogin> implements UserLoginService {

    @Autowired
    private UserLoginMapper userLoginMapper;
    /**
     * 获取用户登录信息
     *
     * @param userId 用户Id
     * @return UserLogin
     */
    @Override
    public List<UserLogin> getUserLogin(String userId) {
        return userLoginMapper.getUserLogin(userId);
    }
}
