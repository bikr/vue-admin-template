package com.beiwangshan.backend.service;

import com.beiwangshan.backend.model.UserLogin;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface UserLoginService extends IService<UserLogin> {

    /**
     * 获取用户登录信息
     * @param userId 用户Id
     * @return UserLogin
     */
    List<UserLogin> getUserLogin(String userId);
}
