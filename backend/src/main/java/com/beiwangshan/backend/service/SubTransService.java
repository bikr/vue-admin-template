package com.beiwangshan.backend.service;

import com.beiwangshan.backend.model.SubTrans;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface SubTransService extends IService<SubTrans> {

    /**
     * 多参数获取子交易
     * @param transCode 交易码
     * @param subTransCode 子交易码
     * @param subTransName 子交易名
     * @param exact 是否精确查询，1-是，0-否，仅在传了subTranName时有效
     * @return SubTrans
     */
    List<SubTrans> getSubTransList(String transCode, String subTransCode, String subTransName,String exact);
}
