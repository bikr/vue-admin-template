package com.beiwangshan.backend.service.impl;

import com.beiwangshan.backend.model.Hispwd;
import com.beiwangshan.backend.mapper.HispwdMapper;
import com.beiwangshan.backend.service.HispwdService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@Service
public class HispwdServiceImpl extends ServiceImpl<HispwdMapper, Hispwd> implements HispwdService {

    @Autowired
    private HispwdMapper hispwdMapper;
    /**
     * 通过用户ID获取用户历史密码
     *
     * @param uerId
     * @return
     */
    @Override
    public Hispwd getHisPwdByUerId(String uerId) {
        return hispwdMapper.getHisPwdByUerId(uerId);
    }
}
