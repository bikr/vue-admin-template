package com.beiwangshan.backend.service.impl;

import com.beiwangshan.backend.model.File;
import com.beiwangshan.backend.mapper.FileMapper;
import com.beiwangshan.backend.service.FileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@Service
public class FileServiceImpl extends ServiceImpl<FileMapper, File> implements FileService {

    @Autowired
    private FileMapper fileMapper;
    /**
     * 通过文件编号，查询文件
     *
     * @param fileNo
     * @return
     */
    @Override
    public File getFileByNo(String fileNo) {
        return fileMapper.getFileByNo(fileNo);
    }
}
