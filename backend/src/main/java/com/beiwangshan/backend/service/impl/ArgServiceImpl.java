package com.beiwangshan.backend.service.impl;

import com.beiwangshan.backend.model.Arg;
import com.beiwangshan.backend.mapper.ArgMapper;
import com.beiwangshan.backend.service.ArgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@Service
public class ArgServiceImpl extends ServiceImpl<ArgMapper, Arg> implements ArgService {

    @Autowired
    private ArgMapper argMapper;
    /**
     * 100003 通过sysCode获取系统参数
     * @param sysCode
     * @return
     */
    @Override
    public Arg getArgBySysCode(String sysCode) {
        return argMapper.getArgBySysCode(sysCode);
    }
}
