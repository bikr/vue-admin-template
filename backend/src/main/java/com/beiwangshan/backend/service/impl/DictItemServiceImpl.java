package com.beiwangshan.backend.service.impl;

import com.beiwangshan.backend.model.DictItem;
import com.beiwangshan.backend.mapper.DictItemMapper;
import com.beiwangshan.backend.service.DictItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@Service
public class DictItemServiceImpl extends ServiceImpl<DictItemMapper, DictItem> implements DictItemService {

    @Autowired
    private DictItemMapper dictItemMapper;

    @Override
    public List<DictItem> getDictItemList(String entryCode) {
        return dictItemMapper.getDictItemList(entryCode);
    }
}
