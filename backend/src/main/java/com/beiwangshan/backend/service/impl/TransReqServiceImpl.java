package com.beiwangshan.backend.service.impl;

import com.beiwangshan.backend.model.TransReq;
import com.beiwangshan.backend.mapper.TransReqMapper;
import com.beiwangshan.backend.service.TransReqService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@Service
public class TransReqServiceImpl extends ServiceImpl<TransReqMapper, TransReq> implements TransReqService {

    @Autowired
    private TransReqMapper transReqMapper;
    /**
     * 多参数交易流水表
     * @param serialNo 交易流水
     * @param transCode 交易码
     * @param branchNo 机构码
     * @param userId 用户ID
     * @param errCode 错误
     * @param beginDate 交易开始日期
     * @param endDate 交易结束日期
     * @param beginTime 交易开始时间
     * @param endTime 交易结束时间
     * @return TransReq
     */
    @Override
    public List<TransReq> getTransReqList(String serialNo, String transCode, String branchNo, String userId, String errCode, String beginDate, String endDate, String beginTime, String endTime) {
        return transReqMapper.getTransReqList(serialNo,transCode,branchNo,userId,errCode,beginDate,endDate,beginTime,endTime);
    }
}
