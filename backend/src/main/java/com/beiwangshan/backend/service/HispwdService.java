package com.beiwangshan.backend.service;

import com.beiwangshan.backend.model.Hispwd;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
public interface HispwdService extends IService<Hispwd> {

    /**
     * 通过用户ID获取用户历史密码
     * @param uerId
     * @return
     */
    Hispwd getHisPwdByUerId(String uerId);
}
