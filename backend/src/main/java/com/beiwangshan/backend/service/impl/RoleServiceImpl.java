package com.beiwangshan.backend.service.impl;

import com.beiwangshan.backend.model.Role;
import com.beiwangshan.backend.mapper.RoleMapper;
import com.beiwangshan.backend.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

    @Autowired
    private RoleMapper roleMapper;
    /**
     * 根据roleCode获取权限数据
     *
     * @param roleCode 权限码
     * @return Role
     */
    @Override
    public Role getRoleByCode(String roleCode) {
        return roleMapper.getRoleByCode(roleCode);
    }
}
