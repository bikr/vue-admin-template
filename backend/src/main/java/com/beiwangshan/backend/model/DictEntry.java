package com.beiwangshan.backend.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@TableName("sys_dict_entry")
public class DictEntry implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 字典条目编号
     */
    @TableId("DICT_ENTRY_CODE")
    private String dictEntryCode;

    /**
     * 字典条目名称
     */
    @TableField("DICT_ENTRY_NAME")
    private String dictEntryName;

    /**
     * 控制标志
     */
    @TableField("CTRL_FLAG")
    private String ctrlFlag;

    /**
     * 备注
     */
    @TableField("REMARK")
    private String remark;

    public String getDictEntryCode() {
        return dictEntryCode;
    }

    public void setDictEntryCode(String dictEntryCode) {
        this.dictEntryCode = dictEntryCode;
    }
    public String getDictEntryName() {
        return dictEntryName;
    }

    public void setDictEntryName(String dictEntryName) {
        this.dictEntryName = dictEntryName;
    }
    public String getCtrlFlag() {
        return ctrlFlag;
    }

    public void setCtrlFlag(String ctrlFlag) {
        this.ctrlFlag = ctrlFlag;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "DictEntry{" +
            "dictEntryCode=" + dictEntryCode +
            ", dictEntryName=" + dictEntryName +
            ", ctrlFlag=" + ctrlFlag +
            ", remark=" + remark +
        "}";
    }
}
