package com.beiwangshan.backend.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@TableName("sys_hispwd")
public class Hispwd implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户名
     */
    @TableId("USER_ID")
    private String userId;

    /**
     * 密码
     */
    @TableField("USER_PWD")
    private String userPwd;

    /**
     * 流水号
     */
    @TableField("SERIAL_NO")
    private String serialNo;

    /**
     * 备用
     */
    @TableField("RESERVE")
    private String reserve;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }
    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }
    public String getReserve() {
        return reserve;
    }

    public void setReserve(String reserve) {
        this.reserve = reserve;
    }

    @Override
    public String toString() {
        return "Hispwd{" +
            "userId=" + userId +
            ", userPwd=" + userPwd +
            ", serialNo=" + serialNo +
            ", reserve=" + reserve +
        "}";
    }
}
