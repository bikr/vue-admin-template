package com.beiwangshan.backend.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@Data
@TableName("sys_kind")
public class Kind implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 分类编号
     */
    @TableId("KIND_CODE")
    private String kindCode;

    /**
     * 分类类型
     * 用于表示此类别的分类类型
     * 取数据字典[SYS_KIND_TYPE]
     * 0-数据字典
     * 1-系统参数
     * 2-标准字段
     * 3-系统资源
     * 4-系统菜单
     */
    @TableField("KIND_TYPE")
    private String kindType;

    /**
     * 分类名称
     */
    @TableField("KIND_NAME")
    private String kindName;

    /**
     * 上级编号
     */
    @TableField("PARENT_CODE")
    private String parentCode;

    /**
     * 助记符
     */
    @TableField("MNEMONIC")
    private String mnemonic;

    /**
     * 树索引码
     */
    @TableField("TREE_IDX")
    private String treeIdx;

    /**
     * 备注
     */
    @TableField("REMARK")
    private String remark;

}
