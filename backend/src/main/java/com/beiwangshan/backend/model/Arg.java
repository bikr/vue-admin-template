package com.beiwangshan.backend.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@TableName("sys_arg")
public class Arg implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 系统代码
     */
    @TableId("SYS_CODE")
    private String sysCode;

    /**
     * 系统名称
     */
    @TableField("SYS_NAME")
    private String sysName;

    /**
     * 上一处理日
     */
    @TableField("PREV_DATE")
    private String prevDate;

    /**
     * 初始化日期，也是当前日期
     */
    @TableField("INIT_DATE")
    private String initDate;

    /**
     * 系统状态
[K_XTZT]
0-正常
1-关闭
2-维护
     */
    @TableField("STATUS")
    private String status;

    /**
     * 系统备份日期
     */
    @TableField("BKUP_DATE")
    private String bkupDate;

    /**
     * 数据库备份日期
     */
    @TableField("DBBKUP_DATE")
    private String dbbkupDate;

    /**
     * 保留域
     */
    @TableField("RESERVE")
    private String reserve;

    public String getSysCode() {
        return sysCode;
    }

    public void setSysCode(String sysCode) {
        this.sysCode = sysCode;
    }
    public String getSysName() {
        return sysName;
    }

    public void setSysName(String sysName) {
        this.sysName = sysName;
    }
    public String getPrevDate() {
        return prevDate;
    }

    public void setPrevDate(String prevDate) {
        this.prevDate = prevDate;
    }
    public String getInitDate() {
        return initDate;
    }

    public void setInitDate(String initDate) {
        this.initDate = initDate;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public String getBkupDate() {
        return bkupDate;
    }

    public void setBkupDate(String bkupDate) {
        this.bkupDate = bkupDate;
    }
    public String getDbbkupDate() {
        return dbbkupDate;
    }

    public void setDbbkupDate(String dbbkupDate) {
        this.dbbkupDate = dbbkupDate;
    }
    public String getReserve() {
        return reserve;
    }

    public void setReserve(String reserve) {
        this.reserve = reserve;
    }

    @Override
    public String toString() {
        return "Arg{" +
            "sysCode=" + sysCode +
            ", sysName=" + sysName +
            ", prevDate=" + prevDate +
            ", initDate=" + initDate +
            ", status=" + status +
            ", bkupDate=" + bkupDate +
            ", dbbkupDate=" + dbbkupDate +
            ", reserve=" + reserve +
        "}";
    }
}
