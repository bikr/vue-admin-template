package com.beiwangshan.backend.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@TableName("sys_param")
public class Param implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 参数字典条目编号
     */
    @TableId("PARAM_CODE")
    private String paramCode;

    /**
     * 参数字典条目名称
     */
    @TableField("PARAM_NAME")
    private String paramName;

    /**
     * 参数字典条目值
     */
    @TableField("PARAM_VALUE")
    private String paramValue;

    /**
     * 参数值名称
     */
    @TableField("VALUE_NAME")
    private String valueName;

    /**
     * 控制标志
     */
    @TableField("CTRL_FLAG")
    private String ctrlFlag;

    /**
     * 允许修改标志
     */
    @TableField("MODI_FLAG")
    private String modiFlag;

    /**
     * 备注
     */
    @TableField("REMARK")
    private String remark;

    public String getParamCode() {
        return paramCode;
    }

    public void setParamCode(String paramCode) {
        this.paramCode = paramCode;
    }
    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }
    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }
    public String getValueName() {
        return valueName;
    }

    public void setValueName(String valueName) {
        this.valueName = valueName;
    }
    public String getCtrlFlag() {
        return ctrlFlag;
    }

    public void setCtrlFlag(String ctrlFlag) {
        this.ctrlFlag = ctrlFlag;
    }
    public String getModiFlag() {
        return modiFlag;
    }

    public void setModiFlag(String modiFlag) {
        this.modiFlag = modiFlag;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "Param{" +
            "paramCode=" + paramCode +
            ", paramName=" + paramName +
            ", paramValue=" + paramValue +
            ", valueName=" + valueName +
            ", ctrlFlag=" + ctrlFlag +
            ", modiFlag=" + modiFlag +
            ", remark=" + remark +
        "}";
    }
}
