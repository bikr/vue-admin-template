package com.beiwangshan.backend.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@TableName("sys_dict_item")
public class DictItem implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 字典项编号
     */
    @TableId("DICT_ITEM_CODE")
    private String dictItemCode;

    /**
     * 字典条目编号，与item_code形成键值对模式，一对多
     */
    @TableField("DICT_ENTRY_CODE")
    private String dictEntryCode;

    /**
     * 字典项名称
     */
    @TableField("DICT_ITEM_NAME")
    private String dictItemName;

    public String getDictItemCode() {
        return dictItemCode;
    }

    public void setDictItemCode(String dictItemCode) {
        this.dictItemCode = dictItemCode;
    }
    public String getDictEntryCode() {
        return dictEntryCode;
    }

    public void setDictEntryCode(String dictEntryCode) {
        this.dictEntryCode = dictEntryCode;
    }
    public String getDictItemName() {
        return dictItemName;
    }

    public void setDictItemName(String dictItemName) {
        this.dictItemName = dictItemName;
    }

    @Override
    public String toString() {
        return "DictItem{" +
            "dictItemCode=" + dictItemCode +
            ", dictEntryCode=" + dictEntryCode +
            ", dictItemName=" + dictItemName +
        "}";
    }
}
