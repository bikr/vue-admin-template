package com.beiwangshan.backend.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@TableName("sys_file")
public class File implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 文件编号
     */
    @TableId("FILE_NO")
    private String fileNo;

    /**
     * 上传时间
     */
    @TableField("UPLOAD_DATE")
    private String uploadDate;

    /**
     * 文件类别
字典[K_WJLB]
0-图片
1-视频
2-公告
     */
    @TableField("FILE_TYPE")
    private String fileType;

    /**
     * 文件标题
     */
    @TableField("FILE_TITLE")
    private String fileTitle;

    /**
     * 文件名、图片名等自定义或自动获取
     */
    @TableField("FILE_NAME")
    private String fileName;

    /**
     * 文件概要
     */
    @TableField("FILE_SUMMARY")
    private String fileSummary;

    /**
     * 文件保留路径
     */
    @TableField("FILE_PATH")
    private String filePath;

    /**
     * 保留域
     */
    @TableField("RESERVE")
    private String reserve;

    public String getFileNo() {
        return fileNo;
    }

    public void setFileNo(String fileNo) {
        this.fileNo = fileNo;
    }
    public String getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(String uploadDate) {
        this.uploadDate = uploadDate;
    }
    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
    public String getFileTitle() {
        return fileTitle;
    }

    public void setFileTitle(String fileTitle) {
        this.fileTitle = fileTitle;
    }
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    public String getFileSummary() {
        return fileSummary;
    }

    public void setFileSummary(String fileSummary) {
        this.fileSummary = fileSummary;
    }
    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    public String getReserve() {
        return reserve;
    }

    public void setReserve(String reserve) {
        this.reserve = reserve;
    }

    @Override
    public String toString() {
        return "File{" +
            "fileNo=" + fileNo +
            ", uploadDate=" + uploadDate +
            ", fileType=" + fileType +
            ", fileTitle=" + fileTitle +
            ", fileName=" + fileName +
            ", fileSummary=" + fileSummary +
            ", filePath=" + filePath +
            ", reserve=" + reserve +
        "}";
    }
}
