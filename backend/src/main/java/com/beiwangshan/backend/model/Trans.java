package com.beiwangshan.backend.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@TableName("sys_trans")
public class Trans implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 交易编号
     */
    @TableId("TRANS_CODE")
    private String transCode;

    /**
     * 交易名称
     */
    @TableField("TRANS_NAME")
    private String transName;

    /**
     * 分类编号
     */
    @TableField("KIND_CODE")
    private String kindCode;

    /**
     * 模块编号
     */
    @TableField("MODEL_CODE")
    private String modelCode;

    /**
     * 备注
     */
    @TableField("REMARK")
    private String remark;

    /**
     * 扩展字段
     */
    @TableField("EXT_FIELD")
    private String extField;

    public String getTransCode() {
        return transCode;
    }

    public void setTransCode(String transCode) {
        this.transCode = transCode;
    }
    public String getTransName() {
        return transName;
    }

    public void setTransName(String transName) {
        this.transName = transName;
    }
    public String getKindCode() {
        return kindCode;
    }

    public void setKindCode(String kindCode) {
        this.kindCode = kindCode;
    }
    public String getModelCode() {
        return modelCode;
    }

    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public String getExtField() {
        return extField;
    }

    public void setExtField(String extField) {
        this.extField = extField;
    }

    @Override
    public String toString() {
        return "Trans{" +
            "transCode=" + transCode +
            ", transName=" + transName +
            ", kindCode=" + kindCode +
            ", modelCode=" + modelCode +
            ", remark=" + remark +
            ", extField=" + extField +
        "}";
    }
}
