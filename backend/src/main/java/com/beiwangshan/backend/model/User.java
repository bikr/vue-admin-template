package com.beiwangshan.backend.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@TableName("sys_user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    @TableId("USER_ID")
    private String userId;

    /**
     * 所属机构
     */
    @TableField("BRANCH_CODE")
    private String branchCode;

    /**
     * 用户名称
     */
    @TableField("USER_NAME")
    private String userName;

    /**
     * 用户密码
     */
    @TableField("USER_PWD")
    private String userPwd;

    /**
     * 用户状态
用于表示此用户状态
取数据字典[SYS_USER_STATUS]
0-正常
1-注销
2-禁用
     */
    @TableField("USER_STATUS")
    private String userStatus;

    /**
     * 锁定状态
用于表示此用户是否被锁定
取数据字典[SYS_LOCK_STATUS]
0-签退
1-登录
2-非正常签退
3-锁定
     */
    @TableField("LOCK_STATUS")
    private String lockStatus;

    /**
     * 创建时间
     */
    @TableField("CREATE_DATE")
    private String createDate;

    /**
     * 最后修改时间
     */
    @TableField("MODIFY_DATE")
    private String modifyDate;

    /**
     * 密码修改时间
     */
    @TableField("PASS_MODIFY_DATE")
    private String passModifyDate;

    /**
     * 扩展字段
     */
    @TableField("EXT_FIELD")
    private String extField;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }
    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }
    public String getLockStatus() {
        return lockStatus;
    }

    public void setLockStatus(String lockStatus) {
        this.lockStatus = lockStatus;
    }
    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
    public String getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(String modifyDate) {
        this.modifyDate = modifyDate;
    }
    public String getPassModifyDate() {
        return passModifyDate;
    }

    public void setPassModifyDate(String passModifyDate) {
        this.passModifyDate = passModifyDate;
    }
    public String getExtField() {
        return extField;
    }

    public void setExtField(String extField) {
        this.extField = extField;
    }

    public User(String userId, String branchCode, String userName, String userPwd, String userStatus, String lockStatus, String createDate, String modifyDate, String passModifyDate, String extField) {
        this.userId = userId;
        this.branchCode = branchCode;
        this.userName = userName;
        this.userPwd = userPwd;
        this.userStatus = userStatus;
        this.lockStatus = lockStatus;
        this.createDate = createDate;
        this.modifyDate = modifyDate;
        this.passModifyDate = passModifyDate;
        this.extField = extField;
    }

    @Override
    public String toString() {
        return "User{" +
            "userId=" + userId +
            ", branchCode=" + branchCode +
            ", userName=" + userName +
            ", userPwd=" + userPwd +
            ", userStatus=" + userStatus +
            ", lockStatus=" + lockStatus +
            ", createDate=" + createDate +
            ", modifyDate=" + modifyDate +
            ", passModifyDate=" + passModifyDate +
            ", extField=" + extField +
        "}";
    }
}
