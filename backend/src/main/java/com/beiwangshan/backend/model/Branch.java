package com.beiwangshan.backend.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@TableName("sys_branch")
public class Branch implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 机构编号
     */
    @TableId("BRANCH_CODE")
    private String branchCode;

    /**
     * 机构级别
用于表示此组织的分类
取数据字典[SYS_BRANCH_LEVEL]
0-总部
1-区部
2-分部
     */
    @TableField("BRANCH_LEVEL")
    private String branchLevel;

    /**
     * 机构名称
     */
    @TableField("BRANCH_NAME")
    private String branchName;

    /**
     * 机构简称
     */
    @TableField("SHORT_NAME")
    private String shortName;

    /**
     * 上级机构代码
     */
    @TableField("PARENT_CODE")
    private String parentCode;

    /**
     * 备注
     */
    @TableField("REMARK")
    private String remark;

    /**
     * 扩展字段
     */
    @TableField("EXT_FIELD")
    private String extField;

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public String getBranchLevel() {
        return branchLevel;
    }

    public void setBranchLevel(String branchLevel) {
        this.branchLevel = branchLevel;
    }
    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }
    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public String getExtField() {
        return extField;
    }

    public void setExtField(String extField) {
        this.extField = extField;
    }

    @Override
    public String toString() {
        return "Branch{" +
            "branchCode=" + branchCode +
            ", branchLevel=" + branchLevel +
            ", branchName=" + branchName +
            ", shortName=" + shortName +
            ", parentCode=" + parentCode +
            ", remark=" + remark +
            ", extField=" + extField +
        "}";
    }
}
