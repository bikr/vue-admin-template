package com.beiwangshan.backend.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@TableName("sys_role_right")
public class RoleRight implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 交易编号
     */
    @TableId("TRANS_CODE")
    private String transCode;

    /**
     * 子交易编号
     */
    @TableField("SUB_TRANS_CODE")
    private String subTransCode;

    /**
     * 授权角色
     */
    @TableField("ROLE_CODE")
    private String roleCode;

    /**
     * 生效时间
     */
    @TableField("BEGIN_DATE")
    private String beginDate;

    /**
     * 失效时间
     */
    @TableField("END_DATE")
    private String endDate;

    /**
     * 授权标志
用于表示该授权的操作授权标志
取数据字典[SYS_RIGHT_FLAG]
1-操作
2-授权
     */
    @TableField("RIGHT_FLAG")
    private String rightFlag;

    /**
     * 分配人
     */
    @TableField("CREATE_BY")
    private String createBy;

    /**
     * 分配时间
     */
    @TableField("CREATE_DATE")
    private String createDate;

    public String getTransCode() {
        return transCode;
    }

    public void setTransCode(String transCode) {
        this.transCode = transCode;
    }
    public String getSubTransCode() {
        return subTransCode;
    }

    public void setSubTransCode(String subTransCode) {
        this.subTransCode = subTransCode;
    }
    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }
    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }
    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
    public String getRightFlag() {
        return rightFlag;
    }

    public void setRightFlag(String rightFlag) {
        this.rightFlag = rightFlag;
    }
    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }
    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    @Override
    public String toString() {
        return "RoleRight{" +
            "transCode=" + transCode +
            ", subTransCode=" + subTransCode +
            ", roleCode=" + roleCode +
            ", beginDate=" + beginDate +
            ", endDate=" + endDate +
            ", rightFlag=" + rightFlag +
            ", createBy=" + createBy +
            ", createDate=" + createDate +
        "}";
    }
}
