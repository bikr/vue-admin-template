package com.beiwangshan.backend.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@TableName("sys_menu")
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 菜单编号
     */
    @TableId("MENU_CODE")
    private String menuCode;

    /**
     * 交易编号
     */
    @TableField("TRANS_CODE")
    private String transCode;

    /**
     * 子交易编号
     */
    @TableField("SUB_TRANS_CODE")
    private String subTransCode;

    /**
     * 菜单名称
     */
    @TableField("MENU_NAME")
    private String menuName;

    /**
     * 菜单图标
     */
    @TableField("MENU_ICON")
    private String menuIcon;

    /**
     * 窗口类型
用于表示点击菜单后展现页面的显示方式
取数据字典[SYS_WINDOW_CATE]
0-工作区TAB页
1-弹出窗口
     */
    @TableField("WINDOW_TYPE")
    private String windowType;

    /**
     * 提示信息
     */
    @TableField("TIP")
    private String tip;

    /**
     * 快捷键
     */
    @TableField("HOT_KEY")
    private String hotKey;

    /**
     * 上级编号
     */
    @TableField("PARENT_CODE")
    private String parentCode;

    /**
     * 序号
用于表示同级菜单下的顺序排列
     */
    @TableField("ORDER_NO")
    private Integer orderNo;

    /**
     * 展开标志
用于表示该菜单是否默认展开
取数据字典[SYS_IS_OPEN]
0-否
1-是
     */
    @TableField("OPEN_FLAG")
    private String openFlag;

    /**
     * 路由地址URL
     */
    @TableField("ROUTER_URL")
    private String routerUrl;

    /**
     * 备注
     */
    @TableField("REMARK")
    private String remark;

    public String getMenuCode() {
        return menuCode;
    }

    public void setMenuCode(String menuCode) {
        this.menuCode = menuCode;
    }
    public String getTransCode() {
        return transCode;
    }

    public void setTransCode(String transCode) {
        this.transCode = transCode;
    }
    public String getSubTransCode() {
        return subTransCode;
    }

    public void setSubTransCode(String subTransCode) {
        this.subTransCode = subTransCode;
    }
    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }
    public String getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(String menuIcon) {
        this.menuIcon = menuIcon;
    }
    public String getWindowType() {
        return windowType;
    }

    public void setWindowType(String windowType) {
        this.windowType = windowType;
    }
    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }
    public String getHotKey() {
        return hotKey;
    }

    public void setHotKey(String hotKey) {
        this.hotKey = hotKey;
    }
    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }
    public Integer getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(Integer orderNo) {
        this.orderNo = orderNo;
    }
    public String getOpenFlag() {
        return openFlag;
    }

    public void setOpenFlag(String openFlag) {
        this.openFlag = openFlag;
    }
    public String getRouterUrl() {
        return routerUrl;
    }

    public void setRouterUrl(String routerUrl) {
        this.routerUrl = routerUrl;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "Menu{" +
            "menuCode=" + menuCode +
            ", transCode=" + transCode +
            ", subTransCode=" + subTransCode +
            ", menuName=" + menuName +
            ", menuIcon=" + menuIcon +
            ", windowType=" + windowType +
            ", tip=" + tip +
            ", hotKey=" + hotKey +
            ", parentCode=" + parentCode +
            ", orderNo=" + orderNo +
            ", openFlag=" + openFlag +
            ", routerUrl=" + routerUrl +
            ", remark=" + remark +
        "}";
    }
}
