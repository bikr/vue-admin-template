package com.beiwangshan.backend.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@TableName("sys_sub_trans")
public class SubTrans implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 交易编号
     */
    @TableId("TRANS_CODE")
    private String transCode;

    /**
     * 子交易编号
     */
    @TableField("SUB_TRANS_CODE")
    private String subTransCode;

    /**
     * 子交易名称
     */
    @TableField("SUB_TRANS_NAME")
    private String subTransName;

    /**
     * 路由地址
     */
    @TableField("ROUTER_URL")
    private String routerUrl;

    /**
     * 控制标志
0-不控制
1-授权控制
     */
    @TableField("CTRL_FLAG")
    private String ctrlFlag;

    /**
     * 登录标志
0-否
1-是
     */
    @TableField("LOGIN_FLAG")
    private String loginFlag;

    /**
     * 备注
     */
    @TableField("REMARK")
    private String remark;

    /**
     * 扩展字段
     */
    @TableField("EXT_FIELD")
    private String extField;

    public String getTransCode() {
        return transCode;
    }

    public void setTransCode(String transCode) {
        this.transCode = transCode;
    }
    public String getSubTransCode() {
        return subTransCode;
    }

    public void setSubTransCode(String subTransCode) {
        this.subTransCode = subTransCode;
    }
    public String getSubTransName() {
        return subTransName;
    }

    public void setSubTransName(String subTransName) {
        this.subTransName = subTransName;
    }
    public String getRouterUrl() {
        return routerUrl;
    }

    public void setRouterUrl(String routerUrl) {
        this.routerUrl = routerUrl;
    }
    public String getCtrlFlag() {
        return ctrlFlag;
    }

    public void setCtrlFlag(String ctrlFlag) {
        this.ctrlFlag = ctrlFlag;
    }
    public String getLoginFlag() {
        return loginFlag;
    }

    public void setLoginFlag(String loginFlag) {
        this.loginFlag = loginFlag;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public String getExtField() {
        return extField;
    }

    public void setExtField(String extField) {
        this.extField = extField;
    }

    @Override
    public String toString() {
        return "SubTrans{" +
            "transCode=" + transCode +
            ", subTransCode=" + subTransCode +
            ", subTransName=" + subTransName +
            ", routerUrl=" + routerUrl +
            ", ctrlFlag=" + ctrlFlag +
            ", loginFlag=" + loginFlag +
            ", remark=" + remark +
            ", extField=" + extField +
        "}";
    }
}
