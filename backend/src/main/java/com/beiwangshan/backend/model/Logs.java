package com.beiwangshan.backend.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@TableName("sys_logs")
public class Logs implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 日志编号
     */
    @TableId("LOG_SERIAL")
    private String logSerial;

    /**
     * 操作用户
     */
    @TableField("USER_ID")
    private String userId;

    /**
     * 交易码
     */
    @TableField("TRANS_CODE")
    private String transCode;

    /**
     * 子交易码
     */
    @TableField("SUB_TRANS_CODE")
    private String subTransCode;

    /**
     * 机构部门编号
     */
    @TableField("BRANCH_NO")
    private String branchNo;

    /**
     * 操作日期
     */
    @TableField("OP_DATE")
    private String opDate;

    /**
     * 操作时间
     */
    @TableField("OP_TIME")
    private String opTime;

    /**
     * IP地址
     */
    @TableField("IP")
    private String ip;

    /**
     * 备注
     */
    @TableField("SUMMARY")
    private String summary;

    public String getLogSerial() {
        return logSerial;
    }

    public void setLogSerial(String logSerial) {
        this.logSerial = logSerial;
    }
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getTransCode() {
        return transCode;
    }

    public void setTransCode(String transCode) {
        this.transCode = transCode;
    }
    public String getSubTransCode() {
        return subTransCode;
    }

    public void setSubTransCode(String subTransCode) {
        this.subTransCode = subTransCode;
    }
    public String getBranchNo() {
        return branchNo;
    }

    public void setBranchNo(String branchNo) {
        this.branchNo = branchNo;
    }
    public String getOpDate() {
        return opDate;
    }

    public void setOpDate(String opDate) {
        this.opDate = opDate;
    }
    public String getOpTime() {
        return opTime;
    }

    public void setOpTime(String opTime) {
        this.opTime = opTime;
    }
    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    @Override
    public String toString() {
        return "Logs{" +
            "logSerial=" + logSerial +
            ", userId=" + userId +
            ", transCode=" + transCode +
            ", subTransCode=" + subTransCode +
            ", branchNo=" + branchNo +
            ", opDate=" + opDate +
            ", opTime=" + opTime +
            ", ip=" + ip +
            ", summary=" + summary +
        "}";
    }
}
