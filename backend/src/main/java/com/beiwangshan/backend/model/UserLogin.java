package com.beiwangshan.backend.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@TableName("sys_user_login")
public class UserLogin implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户代码
     */
    @TableId("USER_ID")
    private String userId;

    /**
     * 上次成功登录日
     */
    @TableField("LAST_LOGIN_DATE")
    private String lastLoginDate;

    /**
     * 上次成功登录时
     */
    @TableField("LAST_LOGIN_TIME")
    private String lastLoginTime;

    /**
     * 最近登录操作IP
     */
    @TableField("LAST_LOGIN_IP")
    private String lastLoginIp;

    /**
     * 登录累计失败次数
     */
    @TableField("LOGIN_FAIL_TIMES")
    private Integer loginFailTimes;

    /**
     * 最后登录失败日
     */
    @TableField("LAST_FAIL_DATE")
    private String lastFailDate;

    /**
     * 扩展字段
     */
    @TableField("EXT_FIELD")
    private String extField;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(String lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }
    public String getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(String lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }
    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }
    public Integer getLoginFailTimes() {
        return loginFailTimes;
    }

    public void setLoginFailTimes(Integer loginFailTimes) {
        this.loginFailTimes = loginFailTimes;
    }
    public String getLastFailDate() {
        return lastFailDate;
    }

    public void setLastFailDate(String lastFailDate) {
        this.lastFailDate = lastFailDate;
    }
    public String getExtField() {
        return extField;
    }

    public void setExtField(String extField) {
        this.extField = extField;
    }

    @Override
    public String toString() {
        return "UserLogin{" +
            "userId=" + userId +
            ", lastLoginDate=" + lastLoginDate +
            ", lastLoginTime=" + lastLoginTime +
            ", lastLoginIp=" + lastLoginIp +
            ", loginFailTimes=" + loginFailTimes +
            ", lastFailDate=" + lastFailDate +
            ", extField=" + extField +
        "}";
    }
}
