package com.beiwangshan.backend.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@TableName("sys_trans_req")
public class TransReq implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 交易流水号
     */
    @TableId("SERIAL_NO")
    private String serialNo;

    /**
     * 交易日期
     */
    @TableField("TRANS_DATE")
    private String transDate;

    /**
     * 交易日期
     */
    @TableField("TRANS_TIME")
    private String transTime;

    /**
     * 交易码
     */
    @TableField("TRANS_CODE")
    private String transCode;

    /**
     * 交易机构
     */
    @TableField("BRANCH_NO")
    private Integer branchNo;

    /**
     * 用户ID
     */
    @TableField("USER_ID")
    private Integer userId;

    /**
     * 返回码
     */
    @TableField("ERR_CODE")
    private String errCode;

    /**
     * 错误信息
     */
    @TableField("ERR_MSG")
    private String errMsg;

    /**
     * 交易操作状态
[K_JYZT]
0-处理中
1-处理完成
2-处理失败
3-未处理
     */
    @TableField("STATUS")
    private String status;

    /**
     * 摘要说明
     */
    @TableField("SUMMARY")
    private String summary;

    /**
     * 保留域
     */
    @TableField("RESERVE")
    private String reserve;

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }
    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }
    public String getTransTime() {
        return transTime;
    }

    public void setTransTime(String transTime) {
        this.transTime = transTime;
    }
    public String getTransCode() {
        return transCode;
    }

    public void setTransCode(String transCode) {
        this.transCode = transCode;
    }
    public Integer getBranchNo() {
        return branchNo;
    }

    public void setBranchNo(Integer branchNo) {
        this.branchNo = branchNo;
    }
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }
    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
    public String getReserve() {
        return reserve;
    }

    public void setReserve(String reserve) {
        this.reserve = reserve;
    }

    @Override
    public String toString() {
        return "TransReq{" +
            "serialNo=" + serialNo +
            ", transDate=" + transDate +
            ", transTime=" + transTime +
            ", transCode=" + transCode +
            ", branchNo=" + branchNo +
            ", userId=" + userId +
            ", errCode=" + errCode +
            ", errMsg=" + errMsg +
            ", status=" + status +
            ", summary=" + summary +
            ", reserve=" + reserve +
        "}";
    }
}
