package com.beiwangshan.backend.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@TableName("sys_task")
public class Task implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 作业代码
     */
    @TableId("TASK_CODE")
    private String taskCode;

    /**
     * 父作业码
     */
    @TableField("PARENT_TASK_CODE")
    private String parentTaskCode;

    /**
     * 作业名称
     */
    @TableField("TASK_NAME")
    private String taskName;

    /**
     * 作业处理类
     */
    @TableField("ACTION_CLASS")
    private String actionClass;

    /**
     * 流水号
     */
    @TableField("SERIAL_NO")
    private String serialNo;

    /**
     * 归属页面页码
0日启页面
1日终页面
2自由页面
3异常单步处理页面
     */
    @TableField("TASK_PAGE")
    private String taskPage;

    /**
     * 允许重做标志
0-不允许
1-允许
     */
    @TableField("REDO_FLAG")
    private String redoFlag;

    /**
     * 延时处理标志
0-无
1-延时
2-定时
3-中断
     */
    @TableField("DELAY_FLAG")
    private String delayFlag;

    /**
     * 延时时间
根据延时标志填写
延时标志=‘0’，表示不作处理，则为空格；
延时标志=‘1’，表示延时，则填具体的延时秒数；
延时标志=‘2’，表示定时，则填当天具体的启动时间，格式为hhmmss；
延时标志=‘3’，表示断点，则为空格；
     */
    @TableField("DELAY_TIME")
    private String delayTime;

    /**
     * 执行状态
[K_PCLZT]
0-未激活
1-已激活
2-作业暂停
3-作业完成
4-作业失败
5-作业中断
Z-正在处理
     */
    @TableField("DEAL_STATUS")
    private String dealStatus;

    /**
     * 执行百分比例
     */
    @TableField("EXCUTE_PERCENTAGE")
    private Integer excutePercentage;

    /**
     * 执行错误码
     */
    @TableField("ERR_CODE")
    private String errCode;

    /**
     * 错误信息
     */
    @TableField("ERR_MSG")
    private String errMsg;

    /**
     * 执行开始时间(秒)
     */
    @TableField("BEGIN_TIME")
    private String beginTime;

    /**
     * 执行结束时间秒)
     */
    @TableField("END_TIME")
    private String endTime;

    /**
     * 执行持续时间秒)
     */
    @TableField("HOLD_TIME")
    private String holdTime;

    /**
     * 最近执行日期
     */
    @TableField("LAST_TRANS_DATE")
    private String lastTransDate;

    /**
     * 传递子作业值
     */
    @TableField("TRANS_TO_SUB")
    private String transToSub;

    /**
     * 传递子作业值2
     */
    @TableField("TRANS_TO_SUB2")
    private String transToSub2;

    /**
     * 备注
     */
    @TableField("RESERVE")
    private String reserve;

    public String getTaskCode() {
        return taskCode;
    }

    public void setTaskCode(String taskCode) {
        this.taskCode = taskCode;
    }
    public String getParentTaskCode() {
        return parentTaskCode;
    }

    public void setParentTaskCode(String parentTaskCode) {
        this.parentTaskCode = parentTaskCode;
    }
    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }
    public String getActionClass() {
        return actionClass;
    }

    public void setActionClass(String actionClass) {
        this.actionClass = actionClass;
    }
    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }
    public String getTaskPage() {
        return taskPage;
    }

    public void setTaskPage(String taskPage) {
        this.taskPage = taskPage;
    }
    public String getRedoFlag() {
        return redoFlag;
    }

    public void setRedoFlag(String redoFlag) {
        this.redoFlag = redoFlag;
    }
    public String getDelayFlag() {
        return delayFlag;
    }

    public void setDelayFlag(String delayFlag) {
        this.delayFlag = delayFlag;
    }
    public String getDelayTime() {
        return delayTime;
    }

    public void setDelayTime(String delayTime) {
        this.delayTime = delayTime;
    }
    public String getDealStatus() {
        return dealStatus;
    }

    public void setDealStatus(String dealStatus) {
        this.dealStatus = dealStatus;
    }
    public Integer getExcutePercentage() {
        return excutePercentage;
    }

    public void setExcutePercentage(Integer excutePercentage) {
        this.excutePercentage = excutePercentage;
    }
    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }
    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }
    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }
    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
    public String getHoldTime() {
        return holdTime;
    }

    public void setHoldTime(String holdTime) {
        this.holdTime = holdTime;
    }
    public String getLastTransDate() {
        return lastTransDate;
    }

    public void setLastTransDate(String lastTransDate) {
        this.lastTransDate = lastTransDate;
    }
    public String getTransToSub() {
        return transToSub;
    }

    public void setTransToSub(String transToSub) {
        this.transToSub = transToSub;
    }
    public String getTransToSub2() {
        return transToSub2;
    }

    public void setTransToSub2(String transToSub2) {
        this.transToSub2 = transToSub2;
    }
    public String getReserve() {
        return reserve;
    }

    public void setReserve(String reserve) {
        this.reserve = reserve;
    }

    @Override
    public String toString() {
        return "Task{" +
            "taskCode=" + taskCode +
            ", parentTaskCode=" + parentTaskCode +
            ", taskName=" + taskName +
            ", actionClass=" + actionClass +
            ", serialNo=" + serialNo +
            ", taskPage=" + taskPage +
            ", redoFlag=" + redoFlag +
            ", delayFlag=" + delayFlag +
            ", delayTime=" + delayTime +
            ", dealStatus=" + dealStatus +
            ", excutePercentage=" + excutePercentage +
            ", errCode=" + errCode +
            ", errMsg=" + errMsg +
            ", beginTime=" + beginTime +
            ", endTime=" + endTime +
            ", holdTime=" + holdTime +
            ", lastTransDate=" + lastTransDate +
            ", transToSub=" + transToSub +
            ", transToSub2=" + transToSub2 +
            ", reserve=" + reserve +
        "}";
    }
}
