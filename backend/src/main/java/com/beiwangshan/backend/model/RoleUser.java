package com.beiwangshan.backend.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author beiwangshan
 * @since 2022-05-15
 */
@TableName("sys_role_user")
public class RoleUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户代码
     */
    @TableField("USER_CODE")
    private String userCode;

    /**
     * 角色编号
     */
    @TableField("ROLE_CODE")
    private String roleCode;

    /**
     * 授权标志
     */
    @TableField("RIGHT_FLAG")
    private String rightFlag;

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }
    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }
    public String getRightFlag() {
        return rightFlag;
    }

    public void setRightFlag(String rightFlag) {
        this.rightFlag = rightFlag;
    }

    @Override
    public String toString() {
        return "RoleUser{" +
            "userCode=" + userCode +
            ", roleCode=" + roleCode +
            ", rightFlag=" + rightFlag +
        "}";
    }
}
