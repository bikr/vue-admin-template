-- sys_user 用户表
CREATE TABLE `VueAdmin`.`sys_user`  (
  `USER_ID` varchar(32) NOT NULL COMMENT '用户ID',
  `BRANCH_CODE` varchar(16) NULL COMMENT '所属机构',
  `USER_NAME` varchar(32) NULL COMMENT '用户名称',
  `USER_PWD` varchar(32) NULL COMMENT '用户密码',
  `USER_STATUS` varchar(8) NULL COMMENT '用户状态\r\n用于表示此用户状态\r\n取数据字典[SYS_USER_STATUS]\r\n0-正常\r\n1-注销\r\n2-禁用',
  `LOCK_STATUS` varchar(8) NULL COMMENT '锁定状态\r\n用于表示此用户是否被锁定\r\n取数据字典[SYS_LOCK_STATUS]\r\n0-签退\r\n1-登录\r\n2-非正常签退\r\n3-锁定',
  `CREATE_DATE` datetime NULL COMMENT '创建时间',
  `MODIFY_DATE` datetime NULL COMMENT '最后修改时间',
  `PASS_MODIFY_DATE` datetime NULL COMMENT '密码修改时间',
  `EXT_FIELD` varchar(255) NULL COMMENT '扩展字段',
  PRIMARY KEY (`USER_ID`)
);
ALTER TABLE `vueadmin`.`sys_user` 
MODIFY COLUMN `CREATE_DATE` varchar(8) NULL DEFAULT NULL COMMENT '创建时间' AFTER `LOCK_STATUS`,
MODIFY COLUMN `MODIFY_DATE` varchar(8) NULL DEFAULT NULL COMMENT '最后修改时间' AFTER `CREATE_DATE`,
MODIFY COLUMN `PASS_MODIFY_DATE` varchar(8) NULL DEFAULT NULL COMMENT '密码修改时间' AFTER `MODIFY_DATE`;

-- sys_role 系统角色表
CREATE TABLE `VueAdmin`.`sys_role`  (
  `ROLE_CODE` varchar(16) NOT NULL COMMENT '角色编号',
  `ROLE_NAME` varchar(32) NULL COMMENT '角色名称',
  `CREATOR` varchar(32) NULL COMMENT '角色创建者\r\n0-操作角色\r\n1-授权角色',
  `REMARK` varchar(255) NULL COMMENT '备注',
  PRIMARY KEY (`ROLE_CODE`)
);

-- sys_role_right 角色权限授权表
CREATE TABLE `VueAdmin`.`sys_role_right`  (
  `TRANS_CODE` varchar(32) NOT NULL COMMENT '交易编号',
  `SUB_TRANS_CODE` varchar(32) NOT NULL COMMENT '子交易编号',
  `ROLE_CODE` varchar(16) NOT NULL COMMENT '授权角色',
  `BEGIN_DATE` datetime NOT NULL COMMENT '生效时间',
  `END_DATE` datetime NOT NULL COMMENT '失效时间',
  `RIGHT_FLAG` varchar(8) NOT NULL COMMENT '授权标志\r\n用于表示该授权的操作授权标志\r\n取数据字典[SYS_RIGHT_FLAG]\r\n1-操作\r\n2-授权',
  `CREATE_BY` varchar(32) NULL COMMENT '分配人',
  `CREATE_DATE` datetime NULL COMMENT '分配时间',
  PRIMARY KEY (`TRANS_CODE`, `SUB_TRANS_CODE`, `ROLE_CODE`)
);

-- sys_user_right 用户权限授权表
CREATE TABLE `VueAdmin`.`sys_user_right`  (
  `USER_CODE` varchar(32) NOT NULL COMMENT '用户代码',
  `ROLE_CODE` varchar(32) NOT NULL COMMENT '角色编号',
  `RIGHT_FLAG` varchar(8) NOT NULL COMMENT '授权标志',
  PRIMARY KEY (`USER_CODE`, `ROLE_CODE`, `RIGHT_FLAG`)
);

-- sys_trans 系统交易表
CREATE TABLE `VueAdmin`.`sys_trans`  (
  `TRANS_CODE` varchar(32) NOT NULL COMMENT '交易编号',
  `TRANS_NAME` varchar(32) NULL COMMENT '交易名称',
  `KIND_CODE` varchar(16) NULL COMMENT '分类编号',
  `MODEL_CODE` varchar(32) NULL COMMENT '模块编号',
  `REMARK` varchar(255) NULL COMMENT '备注',
  `EXT_FIELD` varchar(255) NULL COMMENT '扩展字段',
  PRIMARY KEY (`TRANS_CODE`)
);

-- sys_user_login 用户登录记录表
CREATE TABLE `VueAdmin`.`sys_user_login`  (
  `USER_ID` varchar(32) NOT NULL COMMENT '用户代码',
  `LAST_LOGIN_DATE` datetime NULL COMMENT '上次成功登录日',
  `LAST_LOGIN_TIME` datetime NULL COMMENT '上次成功登录时',
  `LAST_LOGIN_IP` varchar(16) NULL COMMENT '最近登录操作IP',
  `LOGIN_FAIL_TIMES` integer NULL COMMENT '登录累计失败次数',
  `LAST_FAIL_DATE` datetime NULL COMMENT '最后登录失败日',
  `EXT_FIELD` varchar(255) NULL COMMENT '扩展字段',
  PRIMARY KEY (`USER_ID`)
);

-- sys_sub_trans 系统子交易码
CREATE TABLE `VueAdmin`.`sys_sub_trans`  (
  `TRANS_CODE` varchar(32) NOT NULL COMMENT '交易编号',
  `SUB_TRANS_CODE` varchar(32) NOT NULL COMMENT '子交易编号',
  `SUB_TRANS_NAME` varchar(255) NULL COMMENT '子交易名称',
  `ROUTER_URL` varchar(255) NULL COMMENT '路由地址',
  `CTRL_FLAG` varchar(8) NULL COMMENT '控制标志\r\n0-不控制\r\n1-授权控制',
  `LOGIN_FLAG` varchar(8) NULL COMMENT '登录标志\r\n0-否\r\n1-是',
  `REMARK` varchar(255) NULL COMMENT '备注',
  `EXT_FIELD` varchar(255) NULL COMMENT '扩展字段',
  PRIMARY KEY (`TRANS_CODE`, `SUB_TRANS_CODE`)
);

-- sys_branch 系统机构部门表
CREATE TABLE `VueAdmin`.`sys_branch`  (
  `BRANCH_CODE` varchar(16) NOT NULL COMMENT '机构编号',
  `BRANCH_LEVEL` varchar(8) NULL COMMENT '机构级别\r\n用于表示此组织的分类\r\n取数据字典[SYS_BRANCH_LEVEL]\r\n0-总部\r\n1-区部\r\n2-分部',
  `BRANCH_NAME` varchar(64) NULL COMMENT '机构名称',
  `SHORT_NAME` varchar(32) NULL COMMENT '机构简称',
  `PARENT_CODE` varchar(16) NULL COMMENT '上级机构代码',
  `REMARK` varchar(255) NULL COMMENT '备注',
  `EXT_FIELD` varchar(255) NULL COMMENT '扩展字段',
  PRIMARY KEY (`BRANCH_CODE`)
);

-- sys_param 系统参数表
CREATE TABLE `VueAdmin`.`sys_param`  (
  `PARAM_CODE` varchar(64) NOT NULL COMMENT '参数字典条目编号',
  `PARAM_NAME` varchar(64) NULL COMMENT '参数字典条目名称',
  `PARAM_VALUE` varchar(255) NULL COMMENT '参数字典条目值',
  `CTRL_FLAG` varchar(8) NULL COMMENT '控制标志',
  `REMARK` varchar(255) NULL COMMENT '备注',
  PRIMARY KEY (`PARAM_CODE`)
);
ALTER TABLE `VueAdmin`.`sys_param` 
ADD COLUMN `VALUE_NAME` varchar(255) NULL COMMENT '参数值名称' AFTER `PARAM_VALUE`,
ADD COLUMN `MODI_FLAG` varchar(1) NULL COMMENT '允许修改标志' AFTER `CTRL_FLAG`;


-- sys_menu 系统菜单表
CREATE TABLE `VueAdmin`.`sys_menu`  (
  `MENU_CODE` varchar(32) NOT NULL COMMENT '菜单编号',
  `TRANS_CODE` varchar(32) NULL COMMENT '交易编号',
  `SUB_TRANS_CODE` varchar(32) NULL COMMENT '子交易编号',
  `MENU_NAME` varchar(64) NULL COMMENT '菜单名称',
  `MENU_ICON` varchar(255) NULL COMMENT '菜单图标',
  `WINDOW_TYPE` varchar(255) NULL COMMENT '窗口类型\r\n用于表示点击菜单后展现页面的显示方式\r\n取数据字典[SYS_WINDOW_CATE]\r\n0-工作区TAB页\r\n1-弹出窗口',
  `TIP` varchar(255) NULL COMMENT '提示信息',
  `HOT_KEY` varchar(8) NULL COMMENT '快捷键',
  `PARENT_CODE` varchar(32) NULL COMMENT '上级编号',
  `ORDER_NO` integer NULL COMMENT '序号\r\n用于表示同级菜单下的顺序排列',
  `OPEN_FLAG` varchar(8) NULL COMMENT '展开标志\r\n用于表示该菜单是否默认展开\r\n取数据字典[SYS_IS_OPEN]\r\n0-否\r\n1-是',
  `ROUTER_URL` varchar(255) NULL COMMENT '路由地址URL',
  `REMARK` varchar(255) NULL COMMENT '备注',
  PRIMARY KEY (`MENU_CODE`)
);

-- sys_role_user 系统用户角色映射表
CREATE TABLE `VueAdmin`.`sys_role_user`  (
  `USER_CODE` varchar(32) NULL COMMENT '用户代码',
  `ROLE_CODE` varchar(16) NULL COMMENT '角色编号',
  `RIGHT_FLAG` varchar(8) NULL COMMENT '授权标志'
);

-- sys_file 系统文件表
CREATE TABLE `VueAdmin`.`sys_file`  (
  `FILE_NO` varchar(32) NOT NULL COMMENT '文件编号',
  `UPLOAD_DATE` datetime NULL COMMENT '上传时间',
  `FILE_TYPE` varchar(8) NULL COMMENT '文件类别\r\n字典[K_WJLB]\r\n0-图片\r\n1-视频\r\n2-公告',
  `FILE_TITLE` varchar(255) NULL COMMENT '文件标题',
  `FILE_NAME` varchar(255) NULL COMMENT '文件名、图片名等自定义或自动获取',
  `FILE_SUMMARY` varchar(255) NULL COMMENT '文件概要',
  `FILE_PATH` varchar(255) NULL COMMENT '文件保留路径',
  `RESERVE` varchar(255) NULL COMMENT '保留域',
  PRIMARY KEY (`FILE_NO`)
);

-- sys_arg 系统参数表
CREATE TABLE `VueAdmin`.`sys_arg`  (
  `SYS_CODE` varchar(32) NOT NULL COMMENT '系统代码',
  `SYS_NAME` varchar(255) NOT NULL COMMENT '系统名称',
  `PREV_DATE` datetime NULL COMMENT '上一处理日',
  `INIT_DATE` datetime NULL COMMENT '初始化日期，也是当前日期',
  `STATUS` varchar(1) NULL COMMENT '系统状态\r\n[K_XTZT]\r\n0-正常\r\n1-关闭\r\n2-维护',
  `BKUP_DATE` datetime NULL COMMENT '系统备份日期',
  `DBBKUP_DATE` datetime NULL COMMENT '数据库备份日期',
  `RESERVE` varchar(255) NULL COMMENT '保留域',
  PRIMARY KEY (`SYS_CODE`)
);

-- sys_hispwd 用户历史密码表
CREATE TABLE `VueAdmin`.`sys_hispwd`  (
  `USER_ID` varchar(32) NOT NULL COMMENT '用户名',
  `USER_PWD` varchar(32) NULL COMMENT '密码',
  `SERIAL_NO` varchar(32) NULL COMMENT '流水号',
  `RESERVE` varchar(255) NULL COMMENT '备用',
  PRIMARY KEY (`USER_ID`)
);

-- sys_dict_item 系统数据字典项
CREATE TABLE `VueAdmin`.`sys_dict_item`  (
  `DICT_ITEM_CODE` varchar(64) NOT NULL COMMENT '字典项编号',
  `DICT_ENTRY_CODE` varchar(64) NOT NULL COMMENT '字典条目编号，与item_code形成键值对模式，一对多',
  `DICT_ITEM_NAME` varchar(255) NULL COMMENT '字典项名称',
  PRIMARY KEY (`DICT_ITEM_CODE`, `DICT_ENTRY_CODE`)
);

-- sys_dict_entry 系统数据字典条目表
CREATE TABLE `VueAdmin`.`sys_dict_entry`  (
  `DICT_ENTRY_CODE` varchar(64) NOT NULL COMMENT '字典条目编号',
  `DICT_ENTRY_NAME` varchar(64) NULL COMMENT '字典条目名称',
  `CTRL_FLAG` varchar(8) NULL COMMENT '控制标志',
  `REMARK` varchar(255) NULL COMMENT '备注',
  PRIMARY KEY (`DICT_ENTRY_CODE`)
);

-- sys_logs 系统日志表
CREATE TABLE `VueAdmin`.`sys_logs`  (
  `LOG_SERIAL` varchar(32) NOT NULL COMMENT '日志编号',
  `USER_ID` varchar(32) NULL COMMENT '操作用户',
  `TRANS_CODE` varchar(32) NULL COMMENT '交易码',
  `SUB_TRANS_CODE` varchar(32) NULL COMMENT '子交易码',
  `BRANCH_NO` varchar(16) NULL COMMENT '机构部门编号',
  `OP_DATE` datetime NULL COMMENT '操作日期',
  `OP_TIME` datetime NULL COMMENT '操作时间',
  `IP` varchar(32) NULL COMMENT 'IP地址',
  `SUMMARY` varchar(255) NULL COMMENT '备注',
  PRIMARY KEY (`LOG_SERIAL`)
);

-- sys_task 系统清算表
CREATE TABLE `VueAdmin`.`sys_task`  (
  `TASK_CODE` varchar(10) NOT NULL COMMENT '作业代码',
  `PARENT_TASK_CODE` varchar(10) NOT NULL COMMENT '父作业码',
  `TASK_NAME` varchar(255) NULL COMMENT '作业名称',
  `ACTION_CLASS` varchar(255) NULL COMMENT '作业处理类',
  `SERIAL_NO` varchar(32) NULL COMMENT '流水号',
  `TASK_PAGE` varchar(1) NULL COMMENT '归属页面页码\r\n0日启页面\r\n1日终页面\r\n2自由页面\r\n3异常单步处理页面',
  `REDO_FLAG` varchar(1) NULL COMMENT '允许重做标志\r\n0-不允许\r\n1-允许',
  `DELAY_FLAG` varchar(1) NULL COMMENT '延时处理标志\r\n0-无\r\n1-延时\r\n2-定时\r\n3-中断',
  `DELAY_TIME` varchar(6) NULL COMMENT '延时时间\r\n根据延时标志填写\r\n延时标志=‘0’，表示不作处理，则为空格；\r\n延时标志=‘1’，表示延时，则填具体的延时秒数；\r\n延时标志=‘2’，表示定时，则填当天具体的启动时间，格式为hhmmss；\r\n延时标志=‘3’，表示断点，则为空格；',
  `DEAL_STATUS` varchar(1) NULL COMMENT '执行状态\r\n[K_PCLZT]\r\n0-未激活\r\n1-已激活\r\n2-作业暂停\r\n3-作业完成\r\n4-作业失败\r\n5-作业中断\r\nZ-正在处理',
  `EXCUTE_PERCENTAGE` integer NULL COMMENT '执行百分比例',
  `ERR_CODE` varchar(6) NULL COMMENT '执行错误码',
  `ERR_MSG` varchar(255) NULL COMMENT '错误信息',
  `BEGIN_TIME` datetime NULL COMMENT '执行开始时间(秒)',
  `END_TIME` datetime NULL COMMENT '执行结束时间秒)',
  `HOLD_TIME` datetime NULL COMMENT '执行持续时间秒)',
  `LAST_TRANS_DATE` datetime NULL COMMENT '最近执行日期',
  `TRANS_TO_SUB` varchar(255) NULL COMMENT '传递子作业值',
  `TRANS_TO_SUB2` varchar(255) NULL DEFAULT NULL COMMENT '传递子作业值2',
  `RESERVE` varchar(255) NULL COMMENT '备注',
  PRIMARY KEY (`TASK_CODE`)
);

-- sys_trans_req 系统交易请求表
CREATE TABLE `VueAdmin`.`sys_trans_req`  (
  `SERIAL_NO` varchar(32) NOT NULL COMMENT '交易流水号',
  `TRANS_DATE` datetime NULL COMMENT '交易日期',
  `TRANS_TIME` datetime NULL COMMENT '交易日期',
  `TRANS_CODE` varchar(255) NULL COMMENT '交易码',
  `BRANCH_NO` int NULL COMMENT '交易机构',
  `USER_ID` int NULL COMMENT '用户ID',
  `ERR_CODE` varchar(6) NULL COMMENT '返回码',
  `ERR_MSG` varchar(255) NULL COMMENT '错误信息',
  `STATUS` varchar(1) NULL COMMENT '交易操作状态\r\n[K_JYZT]\r\n0-处理中\r\n1-处理完成\r\n2-处理失败\r\n3-未处理',
  `SUMMARY` varchar(255) NULL COMMENT '摘要说明',
  `RESERVE` varchar(255) NULL COMMENT '保留域',
  PRIMARY KEY (`SERIAL_NO`)
);

ALTER TABLE `vueadmin`.`sys_arg` 
MODIFY COLUMN `PREV_DATE` varchar(8) NULL DEFAULT NULL COMMENT '上一处理日' AFTER `SYS_NAME`,
MODIFY COLUMN `INIT_DATE` varchar(8) NULL DEFAULT NULL COMMENT '初始化日期，也是当前日期' AFTER `PREV_DATE`,
MODIFY COLUMN `BKUP_DATE` varchar(8) NULL DEFAULT NULL COMMENT '系统备份日期' AFTER `STATUS`,
MODIFY COLUMN `DBBKUP_DATE` varchar(8) NULL DEFAULT NULL COMMENT '数据库备份日期' AFTER `BKUP_DATE`;

ALTER TABLE `vueadmin`.`sys_logs` 
MODIFY COLUMN `OP_DATE` varchar(8) NULL DEFAULT NULL COMMENT '操作日期' AFTER `BRANCH_NO`,
MODIFY COLUMN `OP_TIME` varchar(8) NULL DEFAULT NULL COMMENT '操作时间' AFTER `OP_DATE`;

ALTER TABLE `vueadmin`.`sys_role_right` 
MODIFY COLUMN `BEGIN_DATE` varchar(8) NOT NULL COMMENT '生效时间' AFTER `ROLE_CODE`,
MODIFY COLUMN `END_DATE` varchar(8) NOT NULL COMMENT '失效时间' AFTER `BEGIN_DATE`,
MODIFY COLUMN `CREATE_DATE` varchar(8) NULL DEFAULT NULL COMMENT '分配时间' AFTER `CREATE_BY`;